import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DateInput from 'react-datepicker';
import moment from 'moment';

class DatePicker extends Component {

  constructor(){
    super();
    this.onChange = this.onChange.bind(this);
    this.onChangeRaw = this.onChangeRaw.bind(this);
  }

  onChange(date){
    const {name, id} = this.props;
    this.props.onChange({name:name, value: date, id: id, isValid: true});
  }

  onChangeRaw(e){
    const {name, id} = this.props;
    let m = moment(e.target.value);
    this.props.onChange({name:name, value: m, id: id, isValid: m.isValid(), rawValue: e.target.value});
  }

  render() {
    const {
      value,
      size,
      name,
      disabled,
      width,
      error,
      dateFormat,
      minDate,
      maxDate,
      showTimeSelect,
      showMonthDropdown,
      showYearDropdown,
      interval,
      customInput,
      isClearable,
      minRealTime,
    } = this.props;
    let className = 'input';
    let style = width ? {width: width} : {};
    let intervalValue = interval ? interval : 15;
    let minhours = minRealTime===false ? 0 : parseInt(moment().format('HH'));
    let minminute = minRealTime===false  ? 0 : parseInt(moment().format('mm'));
    if (size) className+=` is-${size}`;
    if (error) className+=' is-danger';
    return (
      <div className="field">
        <div className="control" style= {style}>
          <DateInput
            selected={value}
            id={name}
            name={name}
            onChange={this.onChange}
            disabled={disabled}
            className={className}
            dateFormat={dateFormat}
            minDate	={minDate}
            maxDate={maxDate}
            onChangeRaw={this.onChangeRaw}
            showMonthDropdown={showMonthDropdown}
            showYearDropdown={showYearDropdown}
            dropdownMode="select"
            showTimeSelect={showTimeSelect}
            timeFormat="HH:mm"
            timeIntervals={intervalValue}
            timeCaption="time"
            autoComplete="off"
            minTime={moment().hours(minhours).minutes(minminute)}
            maxTime={moment().hours(23).minutes(59)}
            customInput={customInput}
            isClearable={isClearable}
          />
        </div>
        {error && <p className="help is-danger">{error}</p>}
      </div>
    );
  }
}

DatePicker.propTypes = {
  value: PropTypes.object,
  name: PropTypes.string,
  error: PropTypes.string,
  dateFormat: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  size: PropTypes.oneOf([
    'small',
    'medium',
    'normal',
    'large'
  ]),
  disabled: PropTypes.bool,
  width: PropTypes.number,
  interval: PropTypes.number,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  showTimeSelect: PropTypes.bool,
  showMonthDropdown: PropTypes.bool,
  showYearDropdown: PropTypes.bool,
  isClearable: PropTypes.bool,
  minRealTime: PropTypes.bool,
};

DatePicker.defaultProps = {
  size: 'small',
  dateFormat:'DD/MM/YYYY',
  onChange() {},
  showMonthDropdown: true,
  showYearDropdown: true,
  width: 220,
  isClearable: true,
  minRealTime:false
};

export default DatePicker;
