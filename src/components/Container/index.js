import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Container extends Component {
  render() {
    let className = 'container';
    let containerStyle = {padding: '0rem 1.5rem 95px 1.5rem'};
    const {fluid, children, style} = this.props;

    if(fluid) className+=' is-fluid';
    if(style) containerStyle = style;

    return<div className={className} style={containerStyle}>{children}</div>;
  }
}

Container.propTypes = {
  fluid: PropTypes.bool,
  style: PropTypes.object
};

export default Container;
