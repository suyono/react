import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

class SelectSearch extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(val){
    const {name, id} = this.props;
    const data = {
      name: name,
      id: id,
      value: val
    };
    this.props.onChange(data);
  }

  render () {
    const {
      width,
      options,
      placeholder,
      isRemoveSelected,
      value,
      error,
      isMulti,
      isSearchable,
      isClearable,
      isLoading,
      disabled,
      size,
    } = this.props;
    var style = {};
    if(error) style = {border: '1px solid red'};
    let columnStyle = {width: width};
    if(size == 'small'){ columnStyle.fontSize = '0.75rem';}
    else if(size == 'medium'){ columnStyle.fontSize = '1.25rem';}
    else if(size == 'large'){ columnStyle.fontSize = '1.5rem';}
    else if(size == 'normal'){ columnStyle.fontSize = '1rem';}

    return (
      <div style={columnStyle}>
        <Select
          multi={isMulti}
          searchable={isSearchable}
          clearable={isClearable}
          isLoading={isLoading}
          onChange={this.onChange}
          options={options}
          placeholder={placeholder}
          removeSelected={isRemoveSelected}
          simpleValue
          value={value}
          style={style}
          disabled={disabled}
        />
        {error && <p className="help is-danger">{error}</p>}
      </div>
    );
  }
}

SelectSearch.propTypes = {
  width: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  name: PropTypes.string,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  options: PropTypes.array,
  placeholder: PropTypes.string,
  isRemoveSelected: PropTypes.bool,
  value: PropTypes.string,
  error: PropTypes.string,
  onChange: PropTypes.func,
  isMulti: PropTypes.bool,
  isSearchable: PropTypes.bool,
  isClearable: PropTypes.bool,
  isLoading: PropTypes.bool,
  disabled: PropTypes.bool,
  size: PropTypes.oneOf([
    'small',
    'medium',
    'large',
    'normal'
  ]),
};

SelectSearch.defaultProps = {
  options: [],
  placeholder: 'Select Option',
  isRemoveSelected: true,
  value: '',
  name: '',
  onChange(){},
  isMulti: false,
  isSearchable: true,
  isClearable: true,
  isLoading: false,
  disabled: false,
  size: 'small'
};

export default SelectSearch;
