import React from 'react';
import { h } from 'react-hyperscript-helpers';

export default class Hoverable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hovered: false,
    };
  }
  render() {
    const { hovered } = this.state;
    const { type = 'span', children } = this.props;
    const hProps = Object.assign({}, this.props);
    delete hProps.type;
    delete hProps.children;
    return h(
      type,
      Object.assign(hProps, {
        onMouseEnter: () => this.setState({ hovered: true }),
        onMouseLeave: () => this.setState({ hovered: false }),
      }),
      [children(hovered)]
    );
  }
}
