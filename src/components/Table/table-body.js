import React, { Component } from 'react';
import PropTypes from 'prop-types';


class TabelBody extends Component {
  render() {
    const {style} = this.props;
    return (
      <tbody style={style} >
        {this.props.children}
      </tbody>
    );
  }
}

TabelBody.propTypes = {
  style: PropTypes.object
};

TabelBody.defaultProps = {
  style: {}
};

export default TabelBody;
