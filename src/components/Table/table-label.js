import React, { Component } from 'react';
import PropTypes from 'prop-types';

class TableLabel extends Component {
  render() {
    const {label, width, children, fontSize, align, fontColor, verticalAlign} = this.props;
    return (
      <th style={{color: fontColor, width: width, fontSize: fontSize, paddingLeft: 14, textAlign: align, verticalAlign: verticalAlign}}>
        {label && <span>{label}</span>}
        {children && children}
      </th>
    );
  }
}

TableLabel.propTypes = {
  label: PropTypes.string,
  width: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  fontSize: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  align: PropTypes.string,
  fontColor: PropTypes.string,
  verticalAlign: PropTypes.string,
};

TableLabel.defaultProps = {
  fontSize: 13,
  align: 'center',
  fontColor: '#fff',
  verticalAlign: 'middle'
};

export default TableLabel;
