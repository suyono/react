import React, { Component } from 'react';
import PropTypes from 'prop-types';

const styles =  {
  table:{
    display: 'block',
    overflowX: 'auto'
  }
};

class Table extends Component {
  render() {
    const {fullwidth, border, striped} = this.props;
    const fullwidthClass = fullwidth ? 'is-fullwidth' : '';
    const borderClass = border ? 'is-bordered' : '';
    const stripedClass = striped ? 'is-striped' : '';
    const screenWidth = window.innerWidth;
    let responsivetable = {};
    if(screenWidth < 911) responsivetable = styles.table;

    return (
      <div className="container-table" style={{overflowX:'auto'}}>
        <table className={`table ${fullwidthClass} ${borderClass} ${stripedClass}`} style={responsivetable}>
          {this.props.children}
        </table>
      </div>
    );
  }
}
Table.propTypes = {
  fullwidth: PropTypes.bool,
  border: PropTypes.bool,
  striped: PropTypes.bool,
};

Table.defaultProps = {
  fullwidth: false,
  border: false,
  striped: false
};

export default Table;
