import React, { Component } from 'react';
import PropTypes from 'prop-types';

class TableValue extends Component {
  render() {
    const {value, bold, paddingLeft, colspan, children, onClick, fontSize, align, color, height, width} = this.props;
    let styleText = {};
    if(color) styleText = {color: color};
    return (
      <td colSpan={colspan} style={{paddingLeft: paddingLeft, fontSize: fontSize, textAlign: align, height: height, width: width}} onClick={() => onClick()}>
        {bold ? (
          <strong><span style={styleText}>{value}</span></strong>
        ) : (
          <span style={styleText}>{value}</span>
        )}
        {children}
      </td>
    );
  }
}

TableValue.propTypes = {
  value: PropTypes.any,
  colspan: PropTypes.string,
  bold: PropTypes.bool,
  onClick: PropTypes.func,
  fontSize: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  align: PropTypes.string,
  color: PropTypes.string,
  height: PropTypes.number,
  width: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  paddingLeft: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ])
};

TableValue.defaultProps = {
  onClick(){},
  fontSize: 13,
  align: 'center',
  paddingLeft: 14
};

export default TableValue;
