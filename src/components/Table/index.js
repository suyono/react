import Table from './table';
import TableBody from './table-body';
import TableHead from './table-head';
import TableValue from './table-value';
import TableLabel from './table-label';
import TableValues from './table-values';

Table.Body = TableBody;
Table.Head = TableHead;
Table.Value = TableValue;
Table.Label = TableLabel;
Table.Values = TableValues;
export default Table;
