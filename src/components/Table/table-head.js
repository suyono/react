import React, { Component } from 'react';
import PropTypes from 'prop-types';

const styles = {
  head: {
    background: '#F0802A'
  }
};

class TabelHead extends Component {
  render() {
    const {style, styleTr} = this.props;
    let styleHead = styles.head;
    if(style) styleHead = {...styleHead, ...style};
    return (
      <thead style={styleHead}>
        <tr style={styleTr}>
          {this.props.children}
        </tr>
      </thead>
    );
  }
}

TabelHead.propTypes = {
  style: PropTypes.object,
  styleTr: PropTypes.object
};

export default TabelHead;
