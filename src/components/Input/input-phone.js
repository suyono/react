import React, { Component } from 'react';
import PropTypes from 'prop-types';

class InputPhone extends Component {

  constructor(props){
    super(props);
    this.state={
      errorInput: false,
      valueError: '',
      messages: this.props.language
    };
    this.validateInput = this.validateInput.bind(this);
    this.onBlur = this.onBlur.bind(this);
  }

  onBlur(e){
    const {value} = e.target;
    const checkValue = value.match(/\d/g).length >= 7 && value.match(/\d/g).length <= 15;
    if(!checkValue){
      this.setState({
        errorInput: true,
        valueError: 'Phone number should be more than 7 digits and less than 15 digits'
      });
      return ;
    }

    this.setState({
      errorInput: false,
      valueError: ''
    });
  }

  validateInput(e){
    const {messages} = this.state;
    const textLength = e.target.textLength;
    const {value} = e.target;
    if(textLength == 1){
      if(value != '+'){
        this.setState({
          errorInput: true,
          valueError: messages.text.errorSpecialChar +' '+ value.slice(-1)
        });
        return ;
      }
    }else if(textLength > 1){
      const parseValue = Number.parseInt(value.slice(-1));
      const checkValue = Number.isInteger(parseValue);
      if(!checkValue){
        this.setState({
          errorInput: true,
          valueError: messages.text.errorSpecialChar +' '+ value.slice(-1)
        });
        return;
      }
    }
    const data = e.target;
    this.setState({
      errorInput: false,
      valueError: ''
    });
    this.props.onChange(data);
  }

  render() {
    const {
      value,
      size,
      name,
      loading,
      disabled,
      placeholder,
      maxLength,
      width,
      index,
      error,
      autocomplete,
      hasIconLeft,
      hasIconRight,
      hasAddon,
      children,
      styleInput
    } = this.props;
    const {messages} = this.state;
    let className = 'input';
    let style = width ? {width: width} : {};
    let styles = styleInput;
    if(width) styles = {width: width, ...styles};
    if (size) className+=` is-${size}`;
    if (error) className+=' is-danger';
    let controlClassName = 'control';
    if(loading) controlClassName+=' is-loading';
    if(hasIconRight) controlClassName+= ' has-icons-right';
    if(hasIconLeft) controlClassName+=' has-icons-left';
    let hasAddonClass = hasAddon ? 'has-addons' : '';
    return (
      <div style={style}>
        <div className={`field ${hasAddonClass}`} style={{marginBottom: '0rem'}}>
          <div className={controlClassName}>
            <input
              value={value}
              name={name}
              className={className}
              type="text"
              maxLength={maxLength}
              placeholder={placeholder}
              disabled={disabled}
              onChange={this.validateInput}
              onBlur={this.onBlur}
              style={styles}
              id={index}
              autoComplete={autocomplete}
            />
            {hasIconRight && hasIconRight}
            {hasIconLeft && hasIconLeft}
          </div>
          {hasAddon &&
            <div className="control input-addons">
              {children}
            </div>
          }
        </div>
        <p className="help" style={{color: '#a1a4a4', fontStyle: 'italic'}}>{messages.text.phoneNumber}</p>
        {!error && this.state.errorInput && <p className="help is-danger">{this.state.valueError}</p> }
        {error && <p className="help is-danger">{error}</p>}
      </div>
    );
  }
}

InputPhone.propTypes = {
  value: PropTypes.string,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  maxLength : PropTypes.number,
  onChange: PropTypes.func.isRequired,
  size: PropTypes.oneOf([
    'small',
    'medium',
    'large',
    'normal'
  ]),
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  width: PropTypes.number,
  autocomplete: PropTypes.string,
  language: PropTypes.object.isRequired,
  hasIconLeft: PropTypes.element,
  hasIconRight: PropTypes.element,
  hasAddon: PropTypes.bool,
  styleInput: PropTypes.object,
};

InputPhone.defaultProps = {
  value: '',
  maxLength: 50,
  size: 'small',
  onChange() {},
  autocomplete: 'off',
  styleInput: {}
};

export default InputPhone;
