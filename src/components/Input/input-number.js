import React, { Component } from 'react';
import PropTypes from 'prop-types';

class InputNumber extends Component {

  constructor(props){
    super(props);
    this.state={
      messages: this.props.language
    };
    this.validateInput = this.validateInput.bind(this);
  }

  validateInput(e){
    if (!e.target.validity.valid) return;
    const value = e.target;
    this.props.onChange(value);
  }

  validatePropsValue(){
    const {value} = this.props;
    const parseValue = Number.parseInt(value);
    const checkValue = Number.isInteger(parseValue);

    if(!checkValue) return false;

    return true;
  }

  render() {
    const {
      value,
      size,
      name,
      loading,
      disabled,
      placeholder,
      maxLength,
      width,
      index,
      error,
      hasText,
      autocomplete,
      hasIconLeft,
      hasIconRight,
      hasAddon,
      children,
      styleInput
    } = this.props;
    const {messages} = this.state;
    const validateValue = this.validatePropsValue(value) ? value : '';
    let className = 'input';
    if (size) className+=` is-${size}`;
    if (error) className+=' is-danger';
    let hasAddonClass = hasAddon ? 'has-addons' : '';
    let controlClassName = 'control';
    if(loading) controlClassName+=' is-loading';
    if(hasIconRight) controlClassName+= ' has-icons-right';
    if(hasIconLeft) controlClassName+=' has-icons-left';
    let style = width ? {width: width} : {};
    let styles = styleInput;
    if(width) styles = {width: width, ...styles};
    return (
      <div style={style}>
        <div className={`field ${hasAddonClass}`} style={{marginBottom: '0rem'}}>
          <div className={controlClassName}>
            <input
              value={validateValue}
              name={name}
              className={className}
              type="text"
              pattern="[0-9]*"
              maxLength={maxLength}
              placeholder={placeholder}
              disabled={disabled}
              onChange={this.validateInput}
              style={styles}
              id={index}
              autoComplete={autocomplete}
            />
            {hasIconRight && hasIconRight}
            {hasIconLeft && hasIconLeft}
          </div>
          {hasAddon &&
            <div className="control input-addons">
              {children}
            </div>
          }
        </div>
        {hasText && <p className="help" style={{color: '#a1a4a4', fontStyle: 'italic'}}>{Object.keys(messages).length > 0 && messages.text ? messages.text.number : 'This field contains only numbers'}</p>}
        {error && <p className="help is-danger">{error}</p>}
      </div>
    );
  }
}

InputNumber.propTypes = {
  value: PropTypes.string,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  maxLength : PropTypes.number,
  onChange: PropTypes.func.isRequired,
  size: PropTypes.oneOf([
    'small',
    'medium',
    'large',
    'normal'
  ]),
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  width: PropTypes.number,
  hasText: PropTypes.bool,
  language: PropTypes.object,
  autocomplete: PropTypes.string,
  hasIconLeft: PropTypes.element,
  hasIconRight: PropTypes.element,
  hasAddon: PropTypes.bool,
  styleInput: PropTypes.object,
};

InputNumber.defaultProps = {
  value: '0',
  maxLength: 12,
  size: 'small',
  onChange() {},
  hasText: true,
  autocomplete: 'off',
  styleInput: {}
};

export default InputNumber;
