import Input from './input';
import InputNumber from './input-number';
import InputFile from './input-file';
import InputSpecialChar from './input-specialchar';
import InputCode from './input-code';
import InputLogin from './inputLogin';
import InputGeolocation from './input-geolocation';
import InputEmail from './input-email';
import InputNumberFormat from './input-numberFormat';
import InputRadio from './input-radio';
import InputCheckbox from './input-checkbox';
import InputAlphanumeric from './input-alphanumeric';
import InputAutocomplete from './input-autocomplete';
import InputDecimal from './input-decimal';


Input.Number = InputNumber;
Input.File = InputFile;
Input.SpecialChar = InputSpecialChar;
Input.InputCode = InputCode;
Input.Login = InputLogin;
Input.Geolocation = InputGeolocation;
Input.Email = InputEmail;
Input.NumberFormat = InputNumberFormat;
Input.Radio = InputRadio;
Input.Checkbox = InputCheckbox;
Input.Alphanumeric = InputAlphanumeric;
Input.Autocomplete = InputAutocomplete;
Input.Decimal = InputDecimal;

export default Input;
