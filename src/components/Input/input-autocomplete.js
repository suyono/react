import React, { Component } from 'react';
import PropTypes from 'prop-types';

class InputAutocomplete extends Component {

  constructor(props){
    super(props);
    this.state = {
      openSuggestion: false,
      errorInput: false,
      valueError: '',
      format: /~/,
      isSelect: false,
      oldValue: '',
      mesages: this.props.language
    };
    this.onChange = this.onChange.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.onBlur = this.onBlur.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const {openSuggestion} = nextProps;
    if(openSuggestion === true){
      this.setState({openSuggestion: true});
    }
  }

  onChange(e){
    this.setState({ isSelect: false });
    const value = e.target;
    const {minCharSuggestion, auto} = this.props;
    if(e.target.value.match(this.state.format)){
      this.setState({ errorInput: true, valueError: e.target.value.slice(-1) });
      return;
    }
    this.setState({ errorInput: false, valueError: '' });
    this.props.onChange(value);

    e.target.value.length < minCharSuggestion ?
      this.setState({openSuggestion: false}) :
      (auto == true ? this.setState({openSuggestion: true}) : this.setState({ openSuggestion: false }));
  }

  validatePropsValue(value){
    const checkValue = value.match(this.state.format);
    if(checkValue) return false;
    return true;
  }

  handleClick(e){
    if (this.node.contains(e.target)) {
      return;
    }
    this.setState({openSuggestion: false});
  }

  onBlur(e){
    const {onSuggestion} = this.props;
    const {isSelect, oldValue} = this.state;
    if(onSuggestion){
      if(!isSelect){
        const data = e.target;
        if(oldValue){
          data.value = oldValue;
        }else{
          data.value = '';
        }
        this.props.onChange(data);
      }
    }
  }

  onKeyDown(){
  }

  componentWillMount(){
    document.addEventListener('click', this.handleClick, false);
  }

  componentWillUnmount(){
    document.removeEventListener('click', this.handleClick, false);
  }

  render() {
    const {
      value,
      size,
      loading,
      disabled,
      placeholder,
      hasAddon,
      suggestions,
      maxLength,
      width,
      children,
      autocomplete,
      hasIconLeft,
      hasIconRight,
      error,
      name,
      index,
      styleInput
    } = this.props;
    const {openSuggestion, messages} = this.state;
    const validateValue = this.validatePropsValue(value) ? value : '';
    let className = 'input';
    let controlClassName = 'control';
    if(openSuggestion) controlClassName+= ' autocomplete-open';
    if(loading) controlClassName+=' is-loading';
    if(hasIconRight) controlClassName+= ' has-icons-right';
    if(hasIconLeft) controlClassName+=' has-icons-left';
    let hasAddonClass = hasAddon ? 'has-addons' : '';
    let style = width ? {width: width} : {};
    let styles = styleInput;
    if(width) styles = {width: width, ...styles};
    let suggestionsLength = suggestions.length;

    if (size) className+=` is-${size}`;
    if (error) className+=' is-danger';
    if (this.state.errorInput) className+=' is-danger';
    return (
      <div style={style}>
        <div className={`field ${hasAddonClass}`}>
          <div className={controlClassName}>
            <div style= {style} ref={node => { this.node = node; }}>
              <input
                value={validateValue}
                name={name}
                className={className}
                type="text"
                maxLength={maxLength}
                placeholder={placeholder}
                disabled={disabled}
                onChange={this.onChange}
                id={index}
                style={styles}
                onKeyDown={this.onKeyDown}
                onBlur={this.onBlur}
                autoComplete={autocomplete}
              />
              {hasIconRight && hasIconRight}
              {hasIconLeft && hasIconLeft}
              <div className="autocomplete-content" style= {style}>
                <div className="autocomplete-list">
                  {suggestionsLength > 0 ? (
                    suggestions.map((suggestion, key) =>
                      <span
                        key={key}
                        className="autocomplete-item"
                        onClick={() =>{
                          this.props.onSelectSuggestion(suggestion, {name: name, id: index});
                          this.setState({openSuggestion: false, isSelect: true, oldValue: suggestion.value});
                        }}
                      >{suggestion.label}</span>
                    )
                  ) : (
                    <span className="autocomplete-item-empty">{messages && messages.error ? messages.error.notFound : 'Data not found'}</span>
                  )}
                </div>
              </div>
            </div>
          </div>
          {hasAddon &&
            <div className="control input-addons">
              {children}
            </div>
          }
        </div>
        {error && <p className="help is-danger">{error}</p>}
        {!error && this.state.errorInput === true && <p className="help is-danger">{messages && messages.text ? messages.text.errorSpecialChar +' '+ this.state.valueError : 'Can\'t Contain '}</p>}
      </div>

    );
  }
}

InputAutocomplete.propTypes = {
  value: PropTypes.string,
  placeholder: PropTypes.string,
  maxLength : PropTypes.number,
  onChange: PropTypes.func.isRequired,
  size: PropTypes.oneOf([
    'small',
    'medium',
    'large',
    'normal'
  ]),
  suggestions: PropTypes.array,
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  hasAddon: PropTypes.bool,
  width: PropTypes.number,
  autocomplete: PropTypes.string,
  hasIconLeft: PropTypes.element,
  hasIconRight: PropTypes.element,
  language: PropTypes.object,
  name: PropTypes.string,
  error: PropTypes.string,
  index: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  minCharSuggestion: PropTypes.number,
  onSelectSuggestion: PropTypes.func.isRequired,
  onSuggestion: PropTypes.bool,
  openSuggestion: PropTypes.bool,
  auto: PropTypes.bool,
  styleInput: PropTypes.object,
};

InputAutocomplete.defaultProps = {
  value: '',
  suggestions: [],
  size: 'small',
  maxLength: 50,
  onChange() {},
  autocomplete: 'off',
  minCharSuggestion: 1,
  onSelectSuggestion(){},
  onSuggestion: false,
  auto: true,
  styleInput: {}
};

export default InputAutocomplete;
