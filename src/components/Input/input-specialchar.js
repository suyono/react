import React, { Component } from 'react';
import PropTypes from 'prop-types';

class InputSpecialChar extends Component {

  constructor(props){
    super(props);
    this.state={
      errorInput: false,
      valueError: '',
      format: /^([a-zA-Z0-9_-]*)$/,
      messages: this.props.language
    };
    this.validateInput = this.validateInput.bind(this);
  }

  validateInput(e){
    const data = e.target;
    if(!data.value.match(this.state.format)){
      this.setState({
        errorInput: true,
        valueError: data.value.slice(-1)
      });
      return;
    }

    this.setState({
      errorInput: false,
      valueError: ''
    });
    this.props.onChange(data);
  }

  validatePropsValue(){
    const {value} = this.props;
    const checkValue = value.match(this.state.format);

    if(checkValue) return true;

    return false;
  }

  render() {
    const {
      value,
      size,
      name,
      loading,
      disabled,
      placeholder,
      maxLength,
      width,
      index,
      error,
      hasAddon,
      children,
      autocomplete,
      hasIconLeft,
      hasIconRight,
      styleInput
    } = this.props;
    const {messages} = this.state;
    const validateValue = this.validatePropsValue(value) ? value : '';
    let className = 'input';
    let controlClassName = 'control';
    if(loading) controlClassName+=' is-loading';
    if(hasIconRight) controlClassName+= ' has-icons-right';
    if(hasIconLeft) controlClassName+=' has-icons-left';
    let hasAddonClass = hasAddon ? 'has-addons' : '';
    let style = width ? {width: width} : {};
    let styles = styleInput;
    if(width) styles = {width: width, ...styles};
    if (size) className+=` is-${size}`;
    if (error) className+=' is-danger';
    return (
      <div style={style}>
        <div className={`field ${hasAddonClass}`}>
          <div className={controlClassName}>
            <input
              value={validateValue}
              name={name}
              className={className}
              type="text"
              maxLength={maxLength}
              placeholder={placeholder}
              disabled={disabled}
              onChange={this.validateInput}
              style={styles}
              id={index}
              autoComplete={autocomplete}
            />
            {hasIconRight && hasIconRight}
            {hasIconLeft && hasIconLeft}
          </div>
          {hasAddon &&
            <div className="control input-addons">
              {children}
            </div>
          }
        </div>
        {!error && this.state.errorInput && <p className="help is-danger">{messages.text.errorSpecialChar +' '+ this.state.valueError}</p> }
        {error && <p className="help is-danger">{error}</p>}
      </div>
    );
  }
}

InputSpecialChar.propTypes = {
  value: PropTypes.string,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  maxLength : PropTypes.number,
  onChange: PropTypes.func.isRequired,
  size: PropTypes.oneOf([
    'small',
    'medium',
    'large',
    'normal'
  ]),
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  width: PropTypes.number,
  hasAddon: PropTypes.bool,
  autocomplete: PropTypes.string,
  language: PropTypes.object.isRequired,
  hasIconLeft: PropTypes.element,
  hasIconRight: PropTypes.element,
  styleInput: PropTypes.object,
};

InputSpecialChar.defaultProps = {
  value: '',
  maxLength: 50,
  size: 'small',
  onChange() {},
  autocomplete: 'off',
  styleInput: {}
};

export default InputSpecialChar;
