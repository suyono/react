import React, { Component } from 'react';
import PropTypes from 'prop-types';

class InputEmail extends Component {

  constructor(){
    super();
    this.state={
      errorInput: false,
      valueError: '',
    };
    this.validateInput = this.validateInput.bind(this);
    this.onBlur = this.onBlur.bind(this);
  }

  onBlur(e){
    const {value} = e.target;
    const email = /^(([^<>()[\]\\.,;:\s@\]"]+(\.[^<>()[\]\\.,;:\s@\\"]+)*)|(\\".+\\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!email.test(value)){
      this.setState({
        errorInput: true,
        valueError: 'Invalid email'
      });
      return ;
    }

    this.setState({
      errorInput: false,
      valueError: ''
    });
  }

  validateInput(e){
    const value = e.target;
    this.props.onChange(value);
    this.setState({
      errorInput: false,
      valueError: ''
    });
  }

  render() {
    const {
      value,
      size,
      name,
      loading,
      disabled,
      placeholder,
      maxLength,
      width,
      index,
      error,
      autocomplete,
      hasIconLeft,
      hasIconRight,
      hasAddon,
      children,
      styleInput
    } = this.props;
    let className = 'input';
    let controlClassName = 'control';
    if(loading) controlClassName+=' is-loading';
    if(hasIconRight) controlClassName+= ' has-icons-right';
    if(hasIconLeft) controlClassName+=' has-icons-left';
    let style = width ? {width: width} : {};
    let styles = styleInput;
    if(width) styles = {width: width, ...styles};
    if (size) className+=` is-${size}`;
    if (error) className+=' is-danger';
    if (this.state.errorInput) className += ' is-danger';
    let hasAddonClass = hasAddon ? 'has-addons' : '';
    return (
      <div style={style}>
        <div className={`field ${hasAddonClass}`} style={{marginBottom: '0rem'}}>
          <div className={controlClassName}>
            <input
              value={value}
              name={name}
              className={className}
              type="email"
              maxLength={maxLength}
              placeholder={placeholder}
              disabled={disabled}
              onChange={this.validateInput}
              onBlur={this.onBlur}
              style={styles}
              id={index}
              autoComplete={autocomplete}
            />
            {hasIconRight && hasIconRight}
            {hasIconLeft && hasIconLeft}
          </div>
          {hasAddon &&
            <div className="control input-addons">
              {children}
            </div>
          }
        </div>
        {!error && this.state.errorInput && <p className="help is-danger">{this.state.valueError}</p> }
        {error && <p className="help is-danger">{error}</p>}
      </div>
    );
  }
}

InputEmail.propTypes = {
  value: PropTypes.string,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  maxLength : PropTypes.number,
  onChange: PropTypes.func.isRequired,
  size: PropTypes.oneOf([
    'small',
    'medium',
    'large',
    'normal'
  ]),
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  width: PropTypes.number,
  autocomplete: PropTypes.string,
  hasIconLeft: PropTypes.element,
  hasIconRight: PropTypes.element,
  hasAddon: PropTypes.bool,
  styleInput: PropTypes.object,
};

InputEmail.defaultProps = {
  value: '0',
  maxLength: 50,
  size: 'small',
  onChange() {},
  autocomplete: 'off',
  styleInput: {}
};

export default InputEmail;
