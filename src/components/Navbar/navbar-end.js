import React, { Component } from 'react';
import PropTypes from 'prop-types';

class NavbarEnd extends Component {
  render() {
    const {date, showUsername, showLogoutButton, onLogout,name, styleItem} = this.props;
    let style = {fontSize: 15};
    if(styleItem) style = {...style, styleItem};
    return (
      <div className="navbar-end">
        {this.props.children}
        {date &&
          <span className="navbar-item " style={style}>
            {date}
          </span>
        }
        {showUsername &&
          <span className="navbar-item " style={style}>
            {name}
          </span>
        }
        {showLogoutButton &&
          <div className="navbar-item has-dropdown is-hoverable" style={style}>
            <a style={{fontSize: 15}} style={{display: 'flex', alignItems: 'center'}}>
              <span className="icon has-text-dark">
                <i className="fa fa-user fa-lg" style={{cursor: 'pointer'}}></i>
              </span>
            </a>
            <div className="navbar-dropdown is-boxed" style={{pointerEvents: 'auto', right: '2px', cursor: 'pointer', left: 'auto'}}>
              <div className="navbar-item " onClick={onLogout}  style={{fontSize: 15}}>
                <span style={style}>Logout</span>
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}

NavbarEnd.propTypes = {
  date: PropTypes.string,
  showUsername: PropTypes.bool,
  showLogoutButton: PropTypes.bool,
  onLogout: PropTypes.func,
  name: PropTypes.string
};

NavbarEnd.defaultProps = {
  showUsername: true,
  showLogoutButton: true,
  onLogout(){},
  name: 'Administrator'
};

export default NavbarEnd;
