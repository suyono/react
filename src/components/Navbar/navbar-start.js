import React, { Component } from 'react';

class NavbarStart extends Component {
  render() {
    return (
      <div className="navbar-start">
        {this.props.children}
      </div>
    );
  }
}

export default NavbarStart;
