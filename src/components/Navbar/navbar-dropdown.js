import React, { Component } from 'react';
import PropTypes from 'prop-types';

class NavbarDropdown extends Component {
  render() {
    const {title, children, isBorder} = this.props;
    let style = {};
    if(isBorder === true) style = {...style, borderBottom: '1px solid'};
    return (
      <div className="navbar-item has-dropdown is-hoverable">
        <a className="navbar-link" style={{fontSize: 15}}>
          <span style={style}>{title}</span>
        </a>
        <div className="navbar-dropdown is-boxed">
          {children}
        </div>
      </div>
    );
  }
}

NavbarDropdown.propTypes = {
  title: PropTypes.string.isRequired,
  isBorder: PropTypes.bool
};

NavbarDropdown.defaultProps = {
  isBorder: false
};

export default NavbarDropdown;
