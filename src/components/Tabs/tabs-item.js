import React, { Component } from 'react';
import PropTypes from 'prop-types';

class TabsItem extends Component {
  render() {
    const {name, title, active, style, height} = this.props;
    let className = '';
    let fontWeight = 'normal';
    if(name == active){
      className = 'is-active';
      fontWeight = 'bold';
    }else{
      className = '';
      fontWeight = '';
    }
    return (
      <li className={className} data-name={name} style={style} >
        <a style={{height: height, fontWeight: fontWeight}}>
          {title}
        </a>
      </li>
    );
  }
}

TabsItem.propTypes = {
  name: PropTypes.string.isRequired,
  title: PropTypes.string,
  active: PropTypes.string,
  style: PropTypes.object,
  height: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ])
};

TabsItem.defaultProps = {
};

export default TabsItem;
