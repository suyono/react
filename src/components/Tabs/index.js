import Tabs from './tabs';
import TabsItem from './tabs-item';

Tabs.Item = TabsItem;
export default Tabs;
