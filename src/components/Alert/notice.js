import React, { Component } from 'react';
import PropTypes from 'prop-types';

const icon = {
  info: 'fa-info-circle',
  success: 'fa-check-circle',
  error: 'fa-exclamation-circle',
  warning: 'fa-exclamation-circle',
};

class Notice extends Component {

  componentDidMount() {
    const props = this.props;
    if (props.autoClose) {
      setTimeout(() => {
        props.onEnd();
      }, props.duration);
    }
  }

  render() {
    const props = this.props;
    const iconType = icon[props.type];
    return (
      <div className={`alert-box ${props.position}`}>
        <div className="alert-box-content" onClick={() => props.onEnd()}>
          <i className={`alert-icon fa ${iconType} ${props.type}`} aria-hidden="true"></i>
          <span>{props.message}</span>
        </div>
      </div>
    );
  }
}

Notice.propTypes = {
  message: PropTypes.string,
  autoClose: PropTypes.bool,
  duration: PropTypes.number,
  onEnd: PropTypes.func,
  position: PropTypes.string,
};

Notice.defaultProps  = {
  autoClose: true,
  duration: 5000,
  position: 'center',
  onEnd() {},
};

export default Notice;
