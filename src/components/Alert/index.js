import React from 'react';
import ReactDOM from 'react-dom';
import Alert from './alert';

let alerts;

function alert(message, type, autoClose, duration, position) {
  if (!alerts) {
    const div = document.createElement('div');
    div.className  = 'alert-content';
    document.body.appendChild(div);
    alerts = ReactDOM.render(<Alert/>, div);
  }
  alerts.addAlert({
    message,
    type,
    autoClose,
    duration,
    position
  });
}

export default {
  info({message, autoClose, duration, position}){
    alert(message, 'info',  autoClose, duration, position);
  },
  success({message, autoClose, duration, position}){
    alert(message, 'success',  autoClose, duration, position);
  },
  error({message, autoClose, duration, position}){
    alert(message, 'error',  autoClose, duration, position);
  },
  warning({message, autoClose, duration, position}){
    alert(message, 'warning',  autoClose, duration, position);
  },
};
