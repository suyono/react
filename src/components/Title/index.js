import React, { Component } from 'react';
import PropTypes from 'prop-types';

const styles = {
  index:{
    marginBottom: 25
  },
  title: {
    fontSize: 14,
    fontWeight: 600
  },
  divider: {
    background: '#dbdbdb',
    border: 'none',
    display: 'block',
    height: 1,
    margin: '0.5rem 0'
  }
};

class Title extends Component {
  render() {
    const {isDivider, marginBottom} = this.props;
    if(marginBottom) styles.index['marginBottom'] = marginBottom;
    return (
      <div style={styles.index}>
        <span style={styles.title}>{this.props.title}</span>
        {isDivider &&
          <hr style={styles.divider}/>
        }
      </div>
    );
  }
}

Title.propTypes = {
  title: PropTypes.string,
  isDivider: PropTypes.bool,
  marginBottom: PropTypes.number
};

Title.defaultProps = {
  title: 'Title',
  isDivider: true,
  marginBottom: 25
};

export default Title;
