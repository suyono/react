import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Table from '../Table';
import LoadIndicator from '../LoadIndicator';

class TableWithSelect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: '',
      messages: this.props.language
    };
  }

  componentWillReceiveProps(nextProps) {
    const {selectedIndex} = nextProps;
    if(selectedIndex !== this.props.selectedIndex){
      this.setState({ selectedIndex: nextProps.selectedIndex });
    }
  }
  onSelected(index) {
    const { selectedIndex } = this.state;
    if (index !== selectedIndex) {
      this.setState({ selectedIndex: index });
      if (this.props.onSelected) {
        this.props.onSelected(index);
      }
    } else {
      this.setState({ selectedIndex: '' });
      if (this.props.onSelected) {
        this.props.onSelected('');
      }
    }
  }
  render() {
    const { headers = [], data = [], fullwidth, striped, border, loading, selectable, customStyleHeader, onClick = () => {} } = this.props;
    const { selectedIndex, messages } = this.state;
    return (
      <Table fullwidth={fullwidth} striped={striped} border={border}>
        <Table.Head styleTr={customStyleHeader}>
          {headers.map((col, colIndex) => (
            <Table.Label
              align={col.align ? col.align: 'left'}
              verticalAlign={customStyleHeader && customStyleHeader.verticalAlign ? customStyleHeader.verticalAlign : undefined}
              key={colIndex}
              width={col.width}
              label={col.label}
              fontColor={customStyleHeader && customStyleHeader.fontColor ? customStyleHeader.fontColor : undefined}
              fontSize={customStyleHeader && customStyleHeader.fontSize ? customStyleHeader.fontSize : undefined}
            />
          ))}
        </Table.Head>
        <Table.Body>
          {loading ? (
            <Table.Values>
              <Table.Value colspan={headers.length.toString()}>
                <LoadIndicator />
              </Table.Value>
            </Table.Values>
          ) : (
            data.length > 0 ? (
              data.map((item, index) => (
                <Table.Values
                  key={index}
                  onClick={() => selectable ?
                    this.onSelected(index) :
                    onClick(item, index)
                  }
                  isSelected={selectedIndex === index ? true : false}
                >
                  {headers.map((col, colIndex) => {
                    let value = item[col.value];
                    if(col.customValue){
                      value = col.customValue(item, index);
                    }
                    return(
                      <Table.Value
                        align={col.align ? col.align: 'left'}
                        key={colIndex}
                        width={col.width}
                        fontSize={customStyleHeader && customStyleHeader.fontSize ? customStyleHeader.fontSize : undefined}
                      >
                        {value}
                      </Table.Value>
                    );
                  })}
                </Table.Values>
              ))
            ):(
              <Table.Values>
                <Table.Value
                  value={messages && messages.text ? messages.text.empty : 'No data found'}
                  colspan={headers.length.toString()}
                  bold
                />
              </Table.Values>
            )
          )}
        </Table.Body>
      </Table>
    );
  }
}

TableWithSelect.propTypes = {
  headers: PropTypes.any.isRequired,
  data: PropTypes.any,
  fullwidth: PropTypes.bool,
  language: PropTypes.object,
  loading: PropTypes.bool,
  selectable: PropTypes.bool,
  customStyleHeader: PropTypes.object,
  striped: PropTypes.bool,
  border: PropTypes.bool
};

TableWithSelect.defaultProps = {
  selectable: true,
  customStyleHeader: {},
  striped: false,
  border: false,
  fullwidth: true,
};

export default TableWithSelect;
