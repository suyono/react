import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Card extends Component {
  render() {
    const {background, color, children} = this.props;
    let style = {};
    if(background) style = {...style, backgroundColor: background};
    if(color) style = {...style, color: color};
    return (
      <div className="card" style={style}>
        {children}
      </div>
    );
  }
}

Card.propTypes = {
  background: PropTypes.string,
  color: PropTypes.string
};

export default Card;
