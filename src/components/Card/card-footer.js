import React, { Component } from 'react';

export default class CardFooter extends Component {
  render() {
    const {style} = this.props;
    return (
      <footer className="card-footer" style={style}>
        {this.props.children}
      </footer>
    );
  }
}
