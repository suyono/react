import React, { Component } from 'react';
import PropTypes from 'prop-types';

class CardImage extends Component {
  render() {
    const {urlImage} = this.props;
    return (
      <div className="card-image">
        <figure className="image is-4by3">
          <img src={urlImage} />
        </figure>
      </div>
    );
  }
}

CardImage.propTypes = {
  urlImage: PropTypes.string.isRequired
};

export default CardImage;
