import React, { Component } from 'react';
import PropTypes from 'prop-types';

class CardHeader extends Component {
  render() {
    const {center, title, children, color, size} = this.props;
    let classHeaderTitle = 'card-header-title';
    let styleHeader = {paddingLeft: 24, fontSize: size};
    if(color) styleHeader = {...styleHeader, color: color};
    if(center) styleHeader = {...styleHeader, justifyContent: 'center'};
    return (
      <header className="card-header">
        <p className={classHeaderTitle} style={styleHeader}>
          {title}
        </p>
        {children}
      </header>
    );
  }
}

CardHeader.propTypes = {
  center: PropTypes.bool,
  title: PropTypes.string.isRequired,
  color: PropTypes.string,
  size: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ])
};

CardHeader.defaultProps = {
  title: '',
  size: '1.5rem',
  center: true
};

export default CardHeader;
