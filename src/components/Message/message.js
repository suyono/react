import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Message extends Component {
  constructor(){
    super();
    this.state={
      show: false
    };
  }

  componentDidMount() {
    const props = this.props;
    if(props.show){
      this.setState({show: true});
      if (props.autoClose) {
        setTimeout(this.onEnd.bind(this), props.duration);
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(nextProps) !== JSON.stringify(this.props)) {
      if (nextProps.show) {
        this.setState({ show: true });
        if (nextProps.autoClose) {
          setTimeout(this.onEnd.bind(this), nextProps.duration);
        }
      }
    }
  }

  onEnd(){
    this.setState({show: false});
  }

  render() {
    const {message, type} = this.props;
    const {show} = this.state;
    if (show) {
      return(
        <div className={`message is-${type}`} style={{fontSize: 13}}>
          <div className="message-body">
            {message}
          </div>
        </div>
      );
    }

    return null;
  }
}

Message.propTypes = {
  show: PropTypes.bool,
  message: PropTypes.string,
  duration: PropTypes.number,
  type: PropTypes.oneOf([
    'success',
    'info',
    'warning',
    'danger'
  ]),
  autoClose: PropTypes.bool
};

Message.defaultProps = {
  type: 'success',
  autoClose: true,
  duration: 5000
};

export default Message;
