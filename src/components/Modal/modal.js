import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Modal extends Component {
  render() {
    const {children, active, onClose, customStyle} = this.props;
    let activeClass = active ? 'is-active' : '';
    let style = {width: 470};
    if(customStyle) style = {...style, ...customStyle};

    if(!active) return null;
    return (
      <div className={`modal ${activeClass}`}>
        <div className="modal-background" onClick={() => onClose()}></div>
        <div className="modal-card" style={style}>
          {children}
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  active: PropTypes.bool,
  onClose: PropTypes.func,
  customStyle: PropTypes.object
};

Modal.defaultProps = {
  active: false,
  onClose(){}
};

export default Modal;
