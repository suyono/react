import React, { Component } from 'react';

class ModalBody extends Component {
  render() {
    const {children} = this.props;
    return (
      <section className="modal-card-body">
        {children}
      </section>
    );
  }
}

export default ModalBody;
