import React, { Component } from 'react';

class ModalFooter extends Component {
  render() {
    const {children} = this.props;
    return (
      <footer
        className="modal-card-foot"
        style={{display: 'flex', flexShrink: 0, justifyContent: 'flex-end'}}
      >
        {children}
      </footer>
    );
  }
}

export default ModalFooter;
