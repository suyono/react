import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ModalHeader extends Component {
  render() {
    const {title, backgroundColor, customStyle, children} = this.props;
    let style = {backgroundColor: backgroundColor};
    if(customStyle) style = {...style, ...customStyle};
    return (
      <header className="modal-card-head" style={style}>
        <p className="modal-card-title">{title}</p>
        {children}
      </header>
    );
  }
}

ModalHeader.propTypes = {
  title: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string
};

ModalHeader.defaultProps = {
  backgroundColor: '#F5F5F5'
};

export default ModalHeader;
