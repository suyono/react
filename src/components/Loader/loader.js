import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Image from '../Image';

class Loader extends Component {
  render() {
    const {show, logo, logoPlaceholder, text} = this.props;
    if (show){
      return (
        <div className="loader-bg" style={{position: 'fixed'}}>
          <div className="loader-content">
            <div className="spinner" style={{display: 'flex', justifyContent: 'center'}}>
              <Image
                placeholder={logoPlaceholder}
                src={logo}
                alt={text}
                height={45}
                width={45}
              />
            </div>
          </div>
        </div>
      );
    }
    return null;
  }
}

Loader.propTypes = {
  show: PropTypes.bool,
  logo: PropTypes.any.isRequired,
  logoPlaceholder: PropTypes.any,
  text: PropTypes.string
};

export default Loader;
