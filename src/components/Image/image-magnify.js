import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Magnifier from 'react-magnifier';

const styles = {
  imgZoomLens: {
    position: 'absolute',
    border: '1px solid #d4d4d4',
    width: 40,
    height: 40
  },
  imgZoomContainer: {
    display: 'flex',
    justifyContent: 'center',
    padding: 10
  }
};

class ImageMagnify extends Component {

  render() {
    const {width, height, urlImg, lensaType, mgWidth, mgHeight, isRotate, rotate} = this.props;
    let container = styles.imgZoomContainer;
    let image = {};
    if(isRotate){
      if(isRotate == true){
        image = {transform: `rotate(${rotate}deg)`};
      }
    }
    return (
      <div style={container} >
        <Magnifier src={urlImg} width={width} height={height}
          mgShape={lensaType}
          mgWidth={mgWidth}
          mgHeight={mgHeight}
          style={image}
        />
      </div>
    );
  }
}

ImageMagnify.propTypes = {
  width: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  height: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  lensaType: PropTypes.oneOf([
    'circle',
    'square'
  ]),
  mgWidth: PropTypes.number,
  mgHeight: PropTypes.number,
  isRotate: PropTypes.bool,
  rotate: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ])
};

ImageMagnify.defaultProps = {
  lensaType: 'circle',
  mgWidth: 150,
  mgHeight: 150
};

export default ImageMagnify;
