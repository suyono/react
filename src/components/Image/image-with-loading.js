import React, { Component } from 'react';
import Image from 'react-shimmer';
import PropTypes from 'prop-types';

class ImageWithLoading extends Component {
  render() {
    const {src, color, duration, width, height, style, onError, onLoad, fallback, delay} = this.props;
    return (
      <Image
        src={src}
        color={color}
        duration={duration}
        width={width}
        height={height}
        style={style}
        onError={onError}
        onLoad={onLoad}
        fallback={fallback}
        delay={delay}
      />
    );
  }
}

ImageWithLoading.propTypes = {
  src: PropTypes.string.isRequired,
  color: PropTypes.string,
  duration: PropTypes.number,
  width: PropTypes.number,
  height: PropTypes.number,
  style: PropTypes.object,
  onError: PropTypes.func,
  onLoad: PropTypes.func,
  fallback: PropTypes.element,
  delay: PropTypes.number,
};

ImageWithLoading.defaultProps = {
  color: '#f6f7f8',
  duration: 1600
};

export default ImageWithLoading;
