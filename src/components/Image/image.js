import React, { Component } from 'react';
import {LazyLoadImage} from 'react-lazy-load-image-component';
import PropTypes from 'prop-types';
import 'react-lazy-load-image-component/src/effects/blur.css';

class Image extends Component {
  render(){
    const {width, height, placeholder, src, alt} = this.props;
    const style = {};
    if(width) style.width = width;
    if(height) style.height = height;
    return(
      <div style={style}>
        <LazyLoadImage
          alt={alt}
          effect="blur"
          src={src}
          height={height}
          width={width}
          placeholderSrc={placeholder}
        />
      </div>
    );
  }

}

Image.propTypes = {
  width: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  height: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  placeholder: PropTypes.any,
  src: PropTypes.any,
  alt: PropTypes.string
};

Image.defaultProps = {
  height: 100
};

export default Image;
