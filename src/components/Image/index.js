import ImageMagnify from './image-magnify';
import Image from './image';
import ImageWithLoading from './image-with-loading';

Image.Magnify = ImageMagnify;
Image.Loading = ImageWithLoading;
export default Image;
