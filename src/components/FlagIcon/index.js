import React, { Component } from 'react';
import FlagIcons from './flag-icon';
import PropTypes from 'prop-types';

class FlagIcon extends Component {
  render () {
    const {code, size, flip, rotate, squared, component, className, styleName} = this.props;
    return (
      <FlagIcons
        code={code}
        size={size}
        flip={flip}
        rotate={rotate}
        squared={squared}
        Component={component}
        className={className}
        styleName={styleName}
      />
    );
  }
}

FlagIcon.propTypes = {
  code: PropTypes.string.isRequired,
  size: PropTypes.string,
  flip: PropTypes.string,
  rotate: PropTypes.number,
  squared: PropTypes.bool,
  component: PropTypes.string,
  className: PropTypes.string,
  styleName: PropTypes.string
};

export default FlagIcon;
