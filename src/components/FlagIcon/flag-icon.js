import * as React from 'react';
import FlagIconFactory from 'react-flag-icon-css';

const FlagIcons = FlagIconFactory(React, { useCssModules: false });

export default FlagIcons;
