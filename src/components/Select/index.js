import Select from './select';
import Option from './option';
import AutoComplete from './autocomplete';

Select.Option = Option;
Select.AutoComplete = AutoComplete;

export default Select;
