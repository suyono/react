import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ColumnContent extends Component {
  render() {
    let {borderBottom, isBorder, id, style} = this.props;
    if(isBorder === true) style = {borderBottom: borderBottom, ...style};
    return (
      <tr style={style} id={id}>
        {this.props.children}
      </tr>
    );
  }
}

ColumnContent.propTypes = {
  borderBottom: PropTypes.string,
  isBorder: PropTypes.bool,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  style: PropTypes.object
};

ColumnContent.defaultProps = {
  borderBottom: '1px solid #efefef',
  isBorder: true,
  style: {}
};

export default ColumnContent;
