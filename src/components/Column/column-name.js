import React, { Component } from 'react';
import PropTypes from 'prop-types';

const styles = {
  label: {
    padding: 14,
    fontSize: 13,
    width: 210,
  }
};

class ColumnName extends Component {
  render() {
    const {padding, width, bold, fontSize, colSpan} = this.props;
    if(padding) styles.label['padding'] = padding;
    if(width) styles.label['width'] = width;
    if(fontSize) styles.label['fontSize'] = fontSize;
    if(bold === true){
      styles.label['fontWeight'] = 'bold';
    }else{
      styles.label['fontWeight'] = 'normal';
    }
    return (
      <td style={styles.label} colSpan={colSpan}>
        {this.props.text}
        {this.props.children}
      </td>
    );
  }
}

ColumnName.propTypes = {
  text: PropTypes.string,
  padding: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  width: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  bold: PropTypes.bool,
  fontSize: PropTypes.number,
  colSpan: PropTypes.string
};

ColumnName.defaultProps = {
  padding: 14,
  width: 210,
  bold: true,
  fontSize: 13
};

export default ColumnName;
