import Column from './column';
import ColumnContent from './column-content';
import ColumnName from './column-name';
import ColumnValue from './column-value';

Column.Content = ColumnContent;
Column.Name = ColumnName;
Column.Value = ColumnValue;
export default Column;
