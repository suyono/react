import SelectBox from './select';
import Option from './option';

SelectBox.Option = Option;

export default SelectBox;
