import React, { Component } from 'react';
import PropTypes from 'prop-types';

const styles = {
  box: {

  }
};

class SelectBox extends Component {

  constructor(){
    super();
    this.onChange = this.onChange.bind(this);
    this.onClick = this.onClick.bind(this);
    this.onDrop = this.onDrop.bind(this);
    this.allowDrop = this.allowDrop.bind(this);
  }

  onChange(e){
    const value = e.target;
    this.props.onChange(value);
  }

  onClick(e){
    const value = e.target;
    this.props.onClick(value);
  }

  onDrop(e){
    e.preventDefault();
    var index=e.dataTransfer.getData('Text');
    this.props.onDrop(e.target, index);
  }

  allowDrop(e){
    e.preventDefault();
    this.props.onDragOver(e.target);
  }

  render() {
    const {
      size,
      name,
      disabled,
      children,
      width,
      height,
      value
    } = this.props;
    if(width) styles.box['width'] = width;
    if(height) styles.box['height'] = height;
    return (
      <div className="field">
        <select
          name={name}
          disabled={disabled}
          style={styles.box}
          onChange={this.onChange}
          value={value}
          size={size}
          onClick={this.onClick}
          onDrop={this.onDrop}
          onDragOver={this.allowDrop}
          id='tagList'
        >
          {children}
        </select>

      </div>
    );
  }
}

SelectBox.propTypes = {
  value: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  onClick: PropTypes.func,
  size: PropTypes.number,
  disabled: PropTypes.bool,
  width: PropTypes.number,
  height: PropTypes.number,
  onDrop: PropTypes.func,
  onDragOver: PropTypes.func

};

SelectBox.defaultProps = {
  value: '',
  onChange() {},
  onClick(){},
  onDrop(){},
  onDragOver(){}
};

export default SelectBox;
