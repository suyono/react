import React, { Component } from 'react';
import ContentLoader from 'react-content-loader';
import PropTypes from 'prop-types';

class ContentLoaderParent extends Component {
  render() {
    const {speed, width, height, backgroundColor, foregroundColor} = this.props;
    return (
      <ContentLoader
        speed={speed}
        width={width}
        height={height}
        viewBox={`0 0 ${width} ${height}`}
        backgroundColor={backgroundColor}
        foregroundColor={foregroundColor}
      >
        {this.props.children}
      </ContentLoader>
    );
  }
}

ContentLoaderParent.propTypes = {
  speed: PropTypes.number,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  backgroundColor: PropTypes.string,
  foregroundColor: PropTypes.string,
};

ContentLoaderParent.defaultProps = {
  speed: 2,
  backgroundColor: '#f1f1f1',
  foregroundColor: '#f9f9f9'
};

export default ContentLoaderParent;
