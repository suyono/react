import React, { Component } from 'react';
import ContentLoader from './content-loader';

class ContentLoaderDetail extends Component {
  render() {
    let columns = [];
    let y = 15;
    for(var i = 0; i < 10; i++){
      columns.push({index: i, y: y});
      if(i%2 != 0){
        y += 40;
      }
    }
    let indents = columns.map(i => {
      if(i.index%2 == 0){
        return(
          <rect x="10" y={i.y} rx="3" ry="3" width="150" height="20" key={i.index} />
        );
      }else{
        return(
          <rect x="174" y={i.y} rx="3" ry="3" width="400" height="20" key={i.index}  />
        );
      }
    });
    return (
      <ContentLoader
        speed={2}
        width={600}
        height={200}
      >
        {indents}
      </ContentLoader>
    );
  }
}

export default ContentLoaderDetail;
