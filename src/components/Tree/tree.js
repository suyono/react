import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Tree, { TreeNode } from 'rc-tree';
import 'rc-tree/assets/index.css';
import cssAnimation from 'css-animation';

const STYLE = `
.collapse {
  overflow: hidden;
  display: block;
}

.collapse-active {
  transition: height 0.3s ease-out;
}
`;

function animate(node, show, done) {
  let height = node.offsetHeight;
  return cssAnimation(node, 'collapse', {
    start() {
      if (!show) {
        node.style.height = `${node.offsetHeight}px`;
      } else {
        height = node.offsetHeight;
        node.style.height = 0;
      }
    },
    active() {
      node.style.height = `${show ? height : 0}px`;
    },
    end() {
      node.style.height = '';
      done();
    }
  });
}

const animation = {
  enter(node, done) {
    return animate(node, true, done);
  },
  leave(node, done) {
    return animate(node, false, done);
  },
  appear(node, done) {
    return animate(node, true, done);
  }
};

class TreeList extends Component {
  constructor(){
    super();
    this.state={
      expandedKeys: [],
      menuTree: [],
      checkedKeys: []
    };
    this.onExpand = this.onExpand.bind(this);
  }

  componentWillMount(){
    this.generateListTree(this.props.data);
  }

  componentWillReceiveProps(nextProps){
    this.generateListTree(nextProps.data);
    this.setState({
      checkedKeys: nextProps.checkedKeys
    });
  }

  generateListTree(data){
    var menuTree = [];
    var expandedKeys = [];
    data.map((nodeParent) => {
      const {code, level} = nodeParent;
      if(level == 0){
        var children = [];
        data.map((nodeChild) => {
          const cLevel = nodeChild.level;
          const parentCode = nodeChild.parentCode;
          if(cLevel == 1 && parentCode == code){
            children.push(nodeChild);
          }
        });
        children.sort((a, b) => {
          return a.index - b.index;
        });
        var parent = nodeParent;
        parent['child'] = children;
        menuTree.push(parent);
        expandedKeys.push(code);
      }
    });
    menuTree.sort((a, b) => {
      return a.index - b.index;
    });
    this.setState({menuTree: menuTree, expandedKeys: expandedKeys});
  }

  onExpand(expandedKeys){
    this.setState({
      expandedKeys: expandedKeys
    });
  }

  render() {
    const {selectable, checkable, onSelect, onCheck,checkStrictly, disabled, draggable, onDragStart, onDragEnter, onDrop, selectedKeys} = this.props;
    const {menuTree, expandedKeys, checkedKeys} = this.state;
    return (
      <div>
        <style dangerouslySetInnerHTML={{ __html: STYLE }}/>
        <Tree
          className=""
          defaultExpandAll
          showLine
          checkStrictly={checkStrictly}
          expandedKeys={expandedKeys}
          onExpand={this.onExpand}
          checkable={checkable}
          onCheck={onCheck}
          checkedKeys={checkedKeys}
          selectable={selectable}
          onSelect={onSelect}
          selectedKeys={selectedKeys}
          showIcon={false}
          openAnimation={animation}
          draggable={draggable}
          onDragStart={onDragStart}
          onDragEnter={onDragEnter}
          onDrop={onDrop}
        >
          {menuTree.map((node) => {
            const {name, code, child} = node;
            return (
              <TreeNode title={name} key={code} disableCheckbox={disabled}>
                {child.map((subNode) => {
                  const subMenuName = subNode.name;
                  const idSubMenu = subNode.code;
                  return (
                    <TreeNode title={subMenuName} key={idSubMenu} disableCheckbox={disabled} isLeaf />
                  );
                })}
              </TreeNode>
            );
          })}
        </Tree>
      </div>
    );
  }
}

TreeList.propTypes = {
  data: PropTypes.array,
  selectable: PropTypes.bool,
  checkable: PropTypes.bool,
  checkedKeys: PropTypes.array,
  disabled: PropTypes.bool,
  onSelect: PropTypes.func,
  draggable: PropTypes.bool,
  onDragStart: PropTypes.func,
  onDragEnter: PropTypes.func,
  onDrop: PropTypes.func,
  selectedKeys: PropTypes.array,
  expandedKeys: PropTypes.array,
  checkStrictly: PropTypes.bool
};

TreeList.defaultProps = {
  data: [],
  selectable: false,
  checkable: false,
  disabled: false,
  onSelect(){},
  draggable: false,
  onDragEnter(){},
  onDragStart(){},
  onDrop(){},
  checkStrictly: false
};

export default TreeList;
