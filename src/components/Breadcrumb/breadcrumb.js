import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router-dom/Link';

import Helpers from '../../helpers';

class Breadcrumb extends Component {
  constructor(){
    super();
    this.state={
      breadcrumbs: []
    };
  }

  componentDidMount(){
    const {pathname, search} = this.props.location;
    var str = pathname;
    var res = str.split('/');
    var arrUrl= [{name: 'Home', url: '/'}];
    var newRes = [];
    res.map(item => {
      if(item !== ''){
        newRes.push(item);
      }
    });
    newRes.map((item, index) => {
      if(newRes.length > 1){
        if((index+1) == newRes.length){
          arrUrl.push({
            name: Helpers.Convert.snakeToSpasiUpperCase(item),
            url: pathname,
            src: search,
            active: true
          });
        }else if(index == 0){
          arrUrl.push({
            name: Helpers.Convert.snakeToSpasiUpperCase(item),
            url: pathname,
            src: search,
            active: false
          });
        }else if(index == 2){
          if(item == 'uploaded_file'){
            var tempUrl = '';
            for(var j=0; j<index+2; j++){
              tempUrl += `${res[j]}/`;
            }
            arrUrl.push({
              name: Helpers.Convert.snakeToSpasiUpperCase(item),
              url:tempUrl,
              src: '',
              active: false
            });
          }else{
            arrUrl.push({
              name: Helpers.Convert.snakeToSpasiUpperCase(item),
              url: pathname,
              src: search,
              active: false
            });
          }
        }else{
          var tmpUrl = '';
          for(var i=0; i<index+2; i++){
            tmpUrl += `${res[i]}/`;
          }
          arrUrl.push({
            name: Helpers.Convert.snakeToSpasiUpperCase(item),
            url: tmpUrl,
            src: '',
            active: false
          });
        }
      }else{
        arrUrl.push({
          name: Helpers.Convert.snakeToSpasiUpperCase(item),
          url: pathname,
          src: search,
          active: true
        });
      }

    });
    this.setState({
      breadcrumbs: arrUrl
    });
  }

  render() {
    const {breadcrumbs} = this.state;
    const fontSize = 13;
    const {location} = this.props;
    return (
      <nav className="breadcrumb has-succeeds-separator" aria-label="breadcrumbs" style={{marginBottom: '0rem', paddingLeft: '2.5rem'}}>
        <ul>
          {breadcrumbs.map((item, index) => {
            if(item.active){
              return(
                <li key={index} className="is-active">
                  <Link to={{
                    pathname: item.url,
                    search: item.src,
                    state: location.state
                  }}
                  style={{fontSize: fontSize}}>{item.name}</Link>
                </li>
              );
            }else{
              return(
                <li key={index}>
                  <Link to={{
                    pathname: item.url,
                    search: item.src,
                    state: location.state
                  }}
                  style={{fontSize: fontSize}}>{item.name}</Link>
                </li>
              );
            }
          })}
        </ul>
      </nav>
    );
  }
}

Breadcrumb.propTypes = {
  location: PropTypes.object.isRequired
};

export default Breadcrumb;
