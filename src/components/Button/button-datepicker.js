import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ButtonDatepicker extends Component{
  render(){
    const {
      onClick,
      id,
      value,
      size,
      outlined,
      loading,
      block,
      disabled,
      width,
      error,
      type
    } = this.props;
    let className = 'button';
    let style = {width: width, justifyContent: 'start'};
    if (disabled == true) style.backgroundColor = 'whitesmoke';
    if (size) className+=` is-${size}`;
    if (outlined) className+=' is-outlined';
    if (loading) className+=' is-loading';
    if (block) className+=' is-fullwidth';
    if (type) className+=' is-${type}';
    if(error){
      if(error != ''){
        style = {...style, border: '1px solid #c0392b'};
      }
    }
    return(
      <button
        className={className}
        onClick={onClick}
        id={id}
        disabled={disabled}
        type="button"
        style={style}
      >
        {value}
      </button>
    );
  }
}

ButtonDatepicker.propTypes = {
  onClick: PropTypes.func,
  id: PropTypes.any,
  value: PropTypes.any,
  size: PropTypes.oneOf([
    'small',
    'medium',
    'normal',
    'large'
  ]),
  type: PropTypes.oneOf([
    'white',
    'light',
    'black',
    'dark',
    'link',
    'primary',
    'info',
    'success',
    'warning',
    'danger'
  ]),
  outlined: PropTypes.bool,
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  block: PropTypes.bool,
};

ButtonDatepicker.defaultProps = {
  size: 'small',
  type: 'primary'
};

export default ButtonDatepicker;
