import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SideNavDropdown extends Component {

  onDropdownClick(e){
    const data = e.target;
    const {id} = data;
    var allDropdownBtn = document.getElementsByClassName('dropdown-btn');
    var dropdownContainer;
    var i;
    for(i=0; i<allDropdownBtn.length; i++){
      dropdownContainer = allDropdownBtn[i].nextElementSibling;
      if(allDropdownBtn[i].id !== id){
        allDropdownBtn[i].classList.remove('active');
        dropdownContainer.style.display = 'none';
      }else{
        allDropdownBtn[i].classList.toggle('active');
        if(dropdownContainer.style.display == 'block'){
          dropdownContainer.style.display = 'none';
        }else{
          dropdownContainer.style.display = 'block';
        }
      }
    }
  }

  render() {
    const {title, children, id} = this.props;
    return (
      <div>
        <button className="dropdown-btn" id={id} onClick={this.onDropdownClick.bind(this)}>{title}
          <i className="fa fa-caret-down"/>
        </button>
        <div className="dropdown-container">
          {children}
        </div>
      </div>
    );
  }
}

SideNavDropdown.propTypes = {
  title: PropTypes.string,
  id: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]).isRequired
};

SideNavDropdown.defaultProps = {
  title: ''
};

export default SideNavDropdown;
