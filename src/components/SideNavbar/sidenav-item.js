import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SidenavItem extends Component {
  render () {
    const {onClick, name} = this.props;
    return (
      <a onClick={onClick}>{name}</a>
    );
  }
}

SidenavItem.propTypes = {
  onClick: PropTypes.func,
  name: PropTypes.string
};

SidenavItem.defaultProps = {
  name: '',
  onClick(){}
};

export default SidenavItem;
