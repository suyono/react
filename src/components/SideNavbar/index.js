import SideNav from './sidenav';
import SideNavStart from './sidenav-start';
import SideNavEnd from './sidenav-end';
import SideNavDropdown from './sidenav-dropdown';
import SideNavItem from './sidenav-item';
import SideNavContainer from './sidenav-container';

SideNav.Start = SideNavStart;
SideNav.End = SideNavEnd;
SideNav.Dropdown = SideNavDropdown;
SideNav.Item = SideNavItem;
SideNav.Container = SideNavContainer;
export default SideNav;
