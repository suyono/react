import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from '../Icon';
import SideNavContainer from './sidenav-container';

class SideNavEnd extends Component {
  render () {
    const {height, customStyle, children, onLogout, logoutName} = this.props;
    var style = {
      justifyContent: 'flex-end',
      height: height
    };
    if(customStyle) style = {...style, ...customStyle};
    return (
      <SideNavContainer customStyle={style}>
        {children}
        <div>
          <a onClick={onLogout}>
            <Icon iconName="fa-sign-out-alt" size="3x"/> {logoutName}
          </a>
        </div>
      </SideNavContainer>
    );
  }
}

SideNavEnd.propTypes = {
  height: PropTypes.string,
  customStyle: PropTypes.object,
  onLogout: PropTypes.func,
  logoutName: PropTypes.string
};

SideNavEnd.defaultProps = {
  height: '50%',
  logoutName: 'Logout'
};

export default SideNavEnd;
