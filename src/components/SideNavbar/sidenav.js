import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SideNav extends Component {
  render () {
    const {widthNav, children, closeNav, isCloseSideNav} = this.props;
    return (
      <div id="mySidenav" className="sidenav" style={{width: widthNav}}>
        {isCloseSideNav && <div className="closebtn" onClick={closeNav}>&times;</div>}
        {children}
      </div>
    );
  }
}

SideNav.propTypes = {
  widthNav: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  closeNav: PropTypes.func,
  isCloseSideNav: PropTypes.bool
};

SideNav.defaultProps = {
  widthNav: 250,
  isCloseSideNav: true
};

export default SideNav;
