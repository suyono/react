import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SideNavContainer from './sidenav-container';

class SideNavStart extends Component {
  render () {
    const {height, bgColor, padding, textColor, customStyle, logo, altLogo, styleLogo, name, date} = this.props;
    var style = {
      height: height,
      backgroundColor: bgColor,
      width: '100%',
      justifyContent: 'flex-end',
      padding: padding,
      color: textColor
    };
    if(customStyle) style = {...style, ...customStyle};
    return (
      <SideNavContainer customStyle={style}>
        {logo && <img src={logo} alt={altLogo} style={styleLogo} />}
        {name && <span>Hi {name}!</span>}
        {date && <span>{date}</span>}
      </SideNavContainer>
    );
  }
}

SideNavStart.propTypes = {
  height: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  bgColor: PropTypes.string,
  padding: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  textColor: PropTypes.string,
  customStyle: PropTypes.object,
  logo: PropTypes.string,
  name: PropTypes.string,
  date: PropTypes.string,
  altLogo: PropTypes.string,
  styleLogo: PropTypes.object
};

SideNavStart.defaultProps = {
  height: 130,
  bgColor: '#424242',
  padding: '10px 15px',
  textColor: '#F5F5F5'
};

export default SideNavStart;
