import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SideNavContainer extends Component {
  render () {
    const {customStyle, children} = this.props;
    var style = {
      display: 'flex',
      flexDirection: 'column'
    };
    if(customStyle) style = {...style, ...customStyle};
    return (
      <div style={style}>
        {children}
      </div>
    );
  }
}

SideNavContainer.propTypes = {
  customStyle: PropTypes.object
};

export default SideNavContainer;
