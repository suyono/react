import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Footer extends Component {
  componentDidMount(){
    document.getElementById('footer').style.display = null;
  }
  render(){
    const {logo, backgroundColor, lineWidth, lineColor, styleLogo,back,nameText,csText} = this.props;
    let styleImage = {height: 55};
    if(styleLogo) styleImage = styleLogo;
    return(
      <footer id="footer" className="footer" style={{backgroundColor: backgroundColor,backgroundImage: `url(${back})`, position: 'fixed', height: 95, bottom: '0', width: '100%', padding: '1.5rem 2rem'}}>
        <div className="columns">
          <div className="column is-10" style={{width: lineWidth,backgroundColor:'transparent'}}>
            <hr style={{backgroundColor: lineColor, height: 2}} />
          </div>
          {logo && <div className="column is-2">
            <img src={logo} style={styleImage}/>
          </div>}
          {!logo && <div className="column is-3" style={{color:'white',marginLeft:'-11%'}}>
            <div style={{marginTop:'23px'}}>
              {nameText}
            </div>
            <div>
              {csText}
            </div>

          </div>}

        </div>
      </footer>
    );
  }
}

Footer.propTypes = {
  logo: PropTypes.string,
  nameText: PropTypes.string,
  csText: PropTypes.string,
  backgroundColor: PropTypes.string,
  lineWidth: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  lineColor: PropTypes.string,
  styleLogo: PropTypes.object
};

Footer.defaultProps = {
  backgroundColor: 'white',
  lineWidth: '90%',

};

export default Footer;
