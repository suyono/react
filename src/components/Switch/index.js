import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Switch extends Component {
  constructor(){
    super();
    this.onChange = this.onChange.bind(this);
  }

  onChange(e){
    const value = e.target;
    this.props.onChange(value);
  }

  render() {
    const { id, value, name, checked, disabled, text } = this.props;
    return (
      <div style={{display: 'flex', alignItems: 'center'}} >
        <label className="switch">
          <input type="checkbox"
            id={id}
            name={name}
            value={value}
            onChange={this.onChange}
            checked={checked}
            disabled={disabled}
          />
          <span className="slider round"></span>
        </label>&nbsp;
        <span style={{paddingLeft: '10px'}} >{text}</span>
      </div>
    );
  }
}

Switch.propTypes = {
  onChange: PropTypes.func.isRequired,
  id: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  checked: PropTypes.bool.isRequired,
  disabled: PropTypes.bool,
  text: PropTypes.string
};

Switch.defaultProps = {
  onChange() {},
  id: '',
  name: '',
  value: '',
  checked: false,
  text: ''
};

export default Switch;
