import React, { Component } from 'react';
import PropTypes from 'prop-types';

const styles = {
  section:{
    background: '##ffffff',
    padding:'0rem 1.5rem 0.5rem 1.5rem',
    marginBottom: 20,
    borde: '1px solid tranparent',
    boxShadow: '0 0.5rem 1rem rgba(0,0,0,.05), inset 0 -1px 0 rgba(0,0,0,.1)'
  },
  title:{
    fontSize: '1.5rem',
    fontWeight: 700,
    lineHeight: 'none'
  }
};

class Header extends Component {
  render() {
    const {center, textColor, title, customStyle} = this.props;
    var styleTitle = styles.title;
    if(center === true){
      styleTitle['textAlign'] = 'center';
    }else{
      styleTitle['textAlign'] = 'left';
    }
    if(textColor) styleTitle.color = textColor;
    if(customStyle) styles.section = {...styles.section, ...customStyle};
    return (
      <section className="section" style={styles.section}>
        <div className="container is-fluid">
          <h3 className="title" style={styleTitle}>{title}</h3>
        </div>
      </section>
    );
  }
}

Header.propTypes = {
  title: PropTypes.string.isRequired,
  center: PropTypes.bool,
  textColor: PropTypes.string,
  customStyle: PropTypes.object
};

Header.defaultProps = {
  title: 'Title',
  center: true,
};

export default Header;
