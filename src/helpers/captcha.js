const captcha = {
  randomAritmatika,
  randomNumeric
};

function randomNumeric(max){
  return Math.ceil(Math.random()*max);
}

function randomAritmatika(){
  var num1 = randomNumeric(20),
    num2 = randomNumeric(20);
  var opindex = randomNumeric(2);
  var strCaptcha, total;
  switch(opindex){
  case 1:
    strCaptcha = num1+' + '+num2+' = ';
    total = (num1 + num2);
    break;
  case 2:
    strCaptcha = num1+' * '+num2+' = ';
    total = (num1 * num2);
    break;
  default:
    strCaptcha = num1+' + '+num2+' = ';
    total = (num1 + num2);
    break;
  }
  return {
    strCaptcha: strCaptcha,
    total: total
  };
}

export default captcha;
