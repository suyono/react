const urlQuery = {
  getQueryString
};

function getQueryString(field, url){
  const href = url ? url : window.location.search;
  const reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
  const string = reg.exec(href);
  return string ? string[1] : null;
}

export default urlQuery;
