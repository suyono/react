// import message from '@/messages/en';
var required = 'This field is required';

const validation = {
  form,
  messages,
  formArray,
  messageArray,
  formSame2Array,
  messageSame2Array,
  formGeolocation,
  objValidationUpdate,
  formNumber,
  messageNumber,
  arrayValidationUpdate,
  same2ArrayNCheckLongPrice,
  messageSame2ArrayNLongPrice,
  checkLongPrice,
  messageCheckLongPrice,
  messageGeolocation,
  checkError
};

Object.size = function(obj) {
  var size = 0, key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) size++;
  }
  return size;
};

function form(errors, form){
  let errorCount = 0;
  Object.keys(errors).map(error=> {
    if (!Array.isArray(form[error])) {
      if(typeof form[error] == 'object'){
        if(form[error] == null){
          errorCount = errorCount + 1;
        }else if(Object.keys(form[error]).length <= 0){
          errorCount = errorCount +1;
        }
      }else{
        if(form[error] === null || form[error] === undefined || form[error].trim() === '') {
          errorCount = errorCount + 1;
        }
      }
    }else if(Array.isArray(form[error])){
      if(form[error].length == 0){
        errorCount = errorCount + 1;
      }
    }
  });

  return errorCount > 0 ? false : true;
}

function messages(errors, form) {
  let newErrors = {};
  Object.keys(errors).map(error=> {
    if (!Array.isArray(form[error])) {
      if(typeof form[error] == 'object'){
        if(form[error] == null){
          errors[error] = required;
        }else if(Object.keys(form[error]).length <= 0){
          errors[error] = required;
        }
      }else if (typeof form[error] == 'string') {
        if(form[error].trim() === '') {
          errors[error] = required;
        }
      }else{
        if(form[error] === null || form[error] === undefined) {
          errors[error] = required;
        }
      }
    }else if(Array.isArray(form[error])){
      if(form[error].length == 0){
        errors[error] = required;
      }
    }
  });
  newErrors = errors;

  return newErrors;
}

function formNumber(errors, form){
  let errorCount = 0;
  var valMinLimit = Number(form[0]['minLimit']);
  var valMaxLimit = Number(form[0]['maxLimit']);
  var valDefaultLimit = Number(form[0]['defaultLimit']);
  Object.keys(errors).map(data => {
    if(form[0][data] <= 0){
      errorCount = errorCount + 1;
    }else{
      if(data == 'minLimit'){
        if(valMaxLimit > 0){
          if(valMinLimit > valMaxLimit){
            errorCount = errorCount + 1;
          }
        }
      }else if(data == 'maxLimit'){
        if(valMinLimit > 0){
          if(valMaxLimit < valMinLimit){
            errorCount = errorCount + 1;
          }
        }
      }else if(data == 'defaultLimit'){
        if(valMinLimit > 0 && valMaxLimit > 0){
          if(valDefaultLimit <= valMinLimit || valDefaultLimit >= valMaxLimit){
            errorCount = errorCount + 1;
          }
        }
      }
    }
  });
  return errorCount > 0 ? false : true;
}

function messageNumber(errors, form){
  let newErrors = {};
  var valMinLimit = Number(form[0]['minLimit']);
  var valMaxLimit = Number(form[0]['maxLimit']);
  var valDefaultLimit = Number(form[0]['defaultLimit']);
  Object.keys(errors).map(data => {
    if(form[0][data] <= 0){
      errors[data] = 'The value should be greater than 0';
    }else{
      if(data == 'minLimit'){
        if(valMaxLimit > 0){
          if(valMinLimit > valMaxLimit){
            errors[data] = 'Min Limit should be more less than Max Limit';
          }
        }
      }else if(data == 'maxLimit'){
        if(valMinLimit > 0){
          if(valMaxLimit < valMinLimit){
            errors[data] = 'Max Limit should be greater than Min Limit';
          }
        }
      }else if(data == 'defaultLimit'){
        if(valMinLimit > 0 && valMaxLimit > 0){
          if(valDefaultLimit <= valMinLimit || valDefaultLimit >= valMaxLimit){
            errors[data] = 'Default Limit should be between Min Limit and Max Limit';
          }
        }
      }
    }
  });
  newErrors = errors;

  return newErrors;
}

function formGeolocation(errorLatLng, form){
  let errorCount = 0;
  Object.keys(errorLatLng).map(error => {
    if(!Array.isArray(form[error])){
      var format;
      if(error == 'lat'){
        format = new RegExp('^-{0,1}((90|90.[0]{1,20}|[0-9]|[1-8][0-9])|(89|[0-9]|[1-8][0-9])[.]{1}[0-9]{1,20}){1}$');
      }else if(error == 'longt'){
        format = new RegExp('^-{0,1}((180|180.[0]{1,20}|[0-9]|([0-9][0-9])|([1][0-7][0-9]))|(179|[0-9]|([0-9][0-9])|([1][0-7][0-9]))[.]{1}[0-9]{1,20}){1}$');
      }
      if(!format.exec(form[error])){
        errorCount = errorCount + 1;
      }
    }
  });

  return errorCount > 0 ? false : true;
}

function messageGeolocation(errorLatLng, form){
  let newErrors = [];
  Object.keys(errorLatLng).map((error) => {
    if(!Array.isArray(form[error])){
      var format;
      if(error == 'lat'){
        format = new RegExp('^-{0,1}((90|90.[0]{1,20}|[0-9]|[1-8][0-9])|(89|[0-9]|[1-8][0-9])[.]{1}[0-9]{1,20}){1}$');
      }else if(error == 'longt'){
        format = new RegExp('^-{0,1}((180|180.[0]{1,20}|[0-9]|([0-9][0-9])|([1][0-7][0-9]))|(179|[0-9]|([0-9][0-9])|([1][0-7][0-9]))[.]{1}[0-9]{1,20}){1}$');
      }
      if(!format.exec(form[error])){
        newErrors[error] = 'Invalid Data';
      }
    }
  });

  return newErrors;
}

function formArray(form, name){
  let errorCount = 0;
  form.map((item, index) => {
    if(form[index][name] == '' || form[index][name] == null || form[index][name] == undefined){
      errorCount += 1;
    }
  });
  return errorCount > 0 ? false : true;
}

function messageArray(form, name){
  let newErrors = [];
  form.map((item, index) => {
    if(form[index][name] == '' || form[index][name] == null || form[index][name] == undefined){
      newErrors[index] = required;
    }else{
      newErrors[index] = '';
    }
  });
  return newErrors;
}

function formSame2Array(form, errors){
  let errorCount = 0;
  errors.map((item, index) => {
    Object.keys(item).map((childItem) => {
      if(form[index][childItem] == '' || form[index][childItem] == null || form[index][childItem] == undefined){
        errorCount += 1;
      }
    });
  });

  return errorCount > 0 ? false : true;
}

function messageSame2Array(form, errors){
  let newErrors = [];
  errors.map((item, index) => {
    newErrors.push(item);
    Object.keys(item).map((childItem) => {
      if(form[index][childItem] == '' || form[index][childItem] == null || form[index][childItem] == undefined){
        newErrors[index][childItem] = required;
      }else{
        newErrors[index][childItem] = '';
      }
    });
  });

  return newErrors;
}

function same2ArrayNCheckLongPrice(form, errors, name, minLength){
  let errorCount = 0;
  errors.map((item, index) => {
    Object.keys(item).map((childItem) => {
      if(form[index][childItem] == '' || form[index][childItem] == null || form[index][childItem] == undefined){
        errorCount += 1;
      }else{
        if(name){
          if(childItem == name){
            if(form[index][name].toString() != ''){
              if(form[index][name].toString().length < minLength){
                errorCount += 1;
              }
            }
          }
        }
      }
    });
  });
  return errorCount > 0 ? false : true;
}

function messageSame2ArrayNLongPrice(form,errors, name, minLength){
  let newErrors = [];
  errors.map((item, index) => {
    newErrors.push(item);
    Object.keys(item).map((childItem) => {
      if(form[index][childItem] == '' || form[index][childItem] == null || form[index][childItem] == undefined){
        newErrors[index][childItem] = required;
      }else{
        if(name){
          if(childItem == name){
            if(form[index][name].toString() != ''){
              if(form[index][name].toString().length < minLength){
                newErrors[index][name] = 'Minimal input 4 digit';
              }else{
                newErrors[index][childItem] = '';
              }
            }
          }else{
            newErrors[index][childItem] = '';
          }
        }else{
          newErrors[index][childItem] = '';
        }
      }

    });
  });
  return newErrors;
}

function checkLongPrice(form, name, minLength){
  let errorCount = 0;
  form.map((item, index) => {
    Object.keys(item).map(childItem => {
      if(childItem == name){
        if(form[index][childItem].toString() != ''){
          if(form[index][childItem].toString().length < minLength){
            if(form[index][childItem] != 0){
              errorCount += 1;
            }
          }
        }
      }
    });
  });
  return errorCount > 0 ? false : true;
}

function messageCheckLongPrice(form, errors, name, minLength){
  let newErrors = [];
  form.map((item, index) => {
    newErrors.push(errors[index]);
    Object.keys(item).map(childItem => {
      if(childItem == name){
        if(form[index][childItem].toString() != ''){
          if(form[index][childItem].toString().length < minLength){
            if(form[index][childItem] != 0){
              newErrors[index][childItem] = 'Minimal input 4 digit';
            }else{
              newErrors[index][childItem] = '';
            }
          }else{
            newErrors[index][childItem] = '';
          }
        }
      }
    });
  });
  return newErrors;
}

function objValidationUpdate(oldForm, newForm){
  for(var i in oldForm) {
    if(Array.isArray(oldForm[i])){
      if(oldForm[i].length !== newForm[i].length){
        return false;
      }else{
        for(var k=0; k<oldForm[i].length; k++){
          if(typeof(oldForm[i][k]) === 'object'){
            for(var key in oldForm[i][k]){
              if(oldForm[i][k][key] != newForm[i][k][key]) return false;
            }
          }else if(typeof(oldForm[i][k]) === 'string'){
            if(oldForm[i][k] != newForm[i][k]) return false;
          }
        }
      }
    }else if(typeof(oldForm[i]) === 'object'){
      for(var j in oldForm[i]){
        if(oldForm[i][j] !== newForm[i][j]) return false;
      }
    }else{
      if(oldForm[i] !== newForm[i])
        return false;
    }

  }
  return true;
}

function arrayValidationUpdate(oldForm, newForm){
  if(oldForm.length !== newForm.length) return false;
  for(var i=0; i<oldForm.length; i++){
    var type = typeof(oldForm[i]);
    if(type === 'object'){
      for(var k in oldForm[i]){
        if(oldForm[i][k] != newForm[i][k]) return false;
      }
    }else if(type === 'string'){
      if(oldForm[i] != newForm[i]) return false;
    }
  }
  return true;
}

function checkError(errors){
  let countError = 0;
  if(Array.isArray(errors)){
    errors.map(item => {
      if(typeof item == 'object'){
        Object.keys(item).map(childItem => {
          if(item[childItem]){
            countError += 1;
          }
        });
      }else{
        if(item){
          countError +=1;
        }
      }
    });
  }
  else if(typeof(errors) == 'object'){
    Object.keys(errors).map(item => {
      if(Array.isArray(errors[item])){
        errors[item].map(childItem => {
          if(typeof childItem == 'object'){
            Object.keys(childItem).map(subChildItem => {
              if(childItem[subChildItem]){
                countError += 1;
              }
            });
          }else{
            if(childItem){
              countError +=1;
            }
          }
        });
      }else if(typeof errors[item] == 'object'){
        Object.keys(errors[item]).map(childItem => {
          if(errors[item][childItem]){
            countError +=1;
          }
        });
      }else if(typeof errors[item] == 'string'){
        if(errors[item]){
          countError += 1;
        }
      }
    });
  }

  return countError > 0 ? false : true;
}

export default validation;
