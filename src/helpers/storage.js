const storage = {
  checkDataStorage,
  getDataStorage,
  getDataDecrypt,
  setDataEncrypt,
  setDataStorage
};

var engine = require('store/src/store-engine');
var storages = [
  require('store/storages/localStorage')
];
var plugins = [
  require('store/plugins/defaults'),
];
var CryptoJS = require('crypto-js');
var store = engine.createStore(storages, plugins);

function checkDataStorage(name){
  if(store.get(name)){ return true; }
  return false;
}

function getDataStorage(name) {
  var data = store.get(name);
  return data;
}

function getDataDecrypt(name, field) {
  var dataStorage = getDataStorage(name);
  var bytes = CryptoJS.AES.decrypt(dataStorage[field], 'Teravin');
  var dataJSON = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
  return dataJSON;
}

function setDataEncrypt(data, name, field, dataStorage) {
  var dataToStore = CryptoJS.AES.encrypt(JSON.stringify(data), 'Teravin');
  var stringDataToStore = dataToStore.toString();
  var objectData = {};
  if(dataStorage){
    objectData = dataStorage;
  }
  objectData[field] = stringDataToStore;
  setDataStorage(name, objectData);
}

function setDataStorage(name, data) {
  store.set(name, data);
}

export default storage;
