import numeral from 'numeral';

const convert = {
  currencyToNumber,
  numberToCurrency,
  convertDate,
  upperCaseFirstLetter,
  snakeToCamelCase,
  camelToSnakeCase,
  snakeToSpasiUpperCase,
  camelToSpasiUpperCase,
  spasiToSnakeCase,
  spasiToCamelCase,
  changeDecimalSeparator
};

function currencyToNumber(data){
  const format = numeral(data);
  const result = format.value();
  return result;
}

function numberToCurrency(data){
  const result = numeral(data).format('0,0.00');
  return result;
}

function convertDate(date){
  const convert = new Date(date).toUTCString();
  return convert;
}

function upperCaseFirstLetter(str){
  return str.replace(/\w\S*/g, (txt) => {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}

function snakeToCamelCase(val){
  return val.replace(/_\w/g, (txt) => {
    return txt[1].toUpperCase();
  });
}

function camelToSnakeCase(val){
  return val.replace(/(?:^|\.?)([A-Z])/g, (x,y) => {
    return '_' + y.toLowerCase();
  }).replace(/^_/, '');
}

function snakeToSpasiUpperCase(val){
  var str = val.toLowerCase();
  return str.replace(/(?:_| |\b)(\w)/g, (txt) => {
    return txt.toUpperCase().replace('_', ' ');
  });
}

function camelToSpasiUpperCase(val){
  return val.replace(/([A-Z])/g, ' $1')
    .replace(/^./, (txt) => {
      return txt.toUpperCase();
    });
}

function spasiToSnakeCase(val){
  return val.replace(/\s+/g, '_').toLowerCase();
}

function spasiToCamelCase(val) {
  return val.replace(/(?:^\w|[A-Z]|\b\w)/g, (word, index) => {
    return index == 0 ? word.toLowerCase() : word.toUpperCase();
  }).replace(/\s+/g, '');
}

function changeDecimalSeparator(value, from, to){
  let valueString = value.toString();
  let newValue = valueString.replace(from?from:'.', to?to:',');
  return newValue;
}

export default convert;
