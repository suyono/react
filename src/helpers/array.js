const array = {
  split,
  dynamicSort,
  onArrangeDataSelect
};

function split(data, index){
  const res = data.split(':');
  return res[index];
}

function dynamicSort(property, sort) {
  var sortOrder = 1;
  if(property[0] === '-') {
    sortOrder = -1;
    property = property.substr(1);
  }

  if(sort == 'asc'){
    return function (a,b) {
      var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * sortOrder;
    };
  }else if(sort == 'desc'){
    return function (a,b) {
      var result = (a[property] > b[property]) ? -1 : (a[property] < b[property]) ? 1 : 0;
      return result * sortOrder;
    };
  }
}

function onArrangeDataSelect(data, valueName, labelName, customLabel){
  const newData = [];
  if(data.length){
    data.map(item => {
      const value = valueName ? item[valueName] : item.id;
      const label = labelName ? (customLabel ? item[labelName]+' - '+item[customLabel] : item[labelName]) : item.value;
      newData.push({
        ...item,
        value: value,
        label: label
      });
    });
  }

  return newData;
}


export default array;
