const authenticaion = {
  csrftoken,
  getToken,
  getDecodedJwttoken,
  setJwttokenToStorage,
  cekDataStorages,
  decryptData,
  getDataStorages
};

var randomToken = require('random-token');
import Cookies from 'js-cookie';

function csrftoken(lengthToken){
  let cookies = randomToken(lengthToken);
  Cookies.set('CSRF-TOKEN', cookies);
  return cookies;
}

var engine = require('store/src/store-engine');
var storages = [
  require('store/storages/sessionStorage'),
  require('store/storages/cookieStorage')
];
var plugins = [
  require('store/plugins/defaults'),
];
var CryptoJS = require('crypto-js');
var jwtDecode = require('jwt-decode');
var store = engine.createStore(storages, plugins);

function decryptData(name){
  var dataStorage = store.get(name);
  var bytes = CryptoJS.AES.decrypt(dataStorage.page, 'Teravin');
  var dataJSON = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
  return dataJSON;
}

function getToken(name){
  var token = decryptData(name).token;
  return token;
}

function getDecodedJwttoken(token){
  var decoded = jwtDecode(token);
  return decoded;
}

function setJwttokenToStorage(data, name, menu){
  var dataToStore = CryptoJS.AES.encrypt(JSON.stringify(data), 'Teravin');
  var stringDataToStore = dataToStore.toString();
  store.set(name, {
    page: stringDataToStore
  });
  store.set('mn', menu);
}

function cekDataStorages(name){
  if(store.get(name)){ return true; }
  return false;
}

function getDataStorages(name){
  var data = store.get(name);
  return data;
}

export default authenticaion;
