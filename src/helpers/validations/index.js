import { run, runAll } from './ruleRunner';
import * as Type from './rules';

const Validations = {
  RunValidate: run,
  RunValidateAll: runAll,
  Type,
};

export default Validations;
