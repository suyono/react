const mustMatch = options => (text, state) => {
  const errorMessage = () => () => {
    if (options.matchField) {
      if (state[options.matchField] === text) {
        return false;
      }
      if (state.touched.indexOf(options.matchField) >= 0) {
        if (options.errorMessage) {
          return options.errorMessage;
        }
        return 'This field does not match';
      }
    }
    return false;
  };

  return errorMessage();
};

export default mustMatch;
