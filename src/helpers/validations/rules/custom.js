const custom = options => value => {
  const errorMessage = () => () => {
    if (options.isError) {
      if (options.isError(value)) {
        if (options.errorMessage) {
          return options.errorMessage;
        }
        return 'This field is invalid';
      }
    }
    return false;
  };

  return errorMessage();
};

export default custom;
