const alpha = options => text => {
  const errorMessage = () => () => {
    const pattern = /^[a-zA-Z]+$/;
    if (text) {
      if (pattern.test(text)) return false;
    }
    if (options) {
      if (options.errorMessage) {
        return options.errorMessage;
      }
    }
    return 'This field may only contain alphabetic characters';
  };

  return errorMessage();
};

export default alpha;
