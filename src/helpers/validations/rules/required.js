const required = options => text => {
  const errorMessage = () => () => {
    if (text) {
      return false;
    }
    if (options) {
      if (options.errorMessage) {
        return options.errorMessage;
      }
    }
    return 'This field is required';
  };

  return errorMessage();
};

export default required;
