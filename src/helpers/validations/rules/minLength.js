const minLength = options => text => {
  const errorMessage = () => () => {
    if (options.length) {
      if (text.length < options.length) {
        if (options.errorMessage) {
          return options.errorMessage;
        }
        return `This field must be at least ${options.length} characters`;
      }
    }
    return false;
  };

  return errorMessage();
};

export default minLength;
