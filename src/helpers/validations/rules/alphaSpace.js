const alphaSpace = options => text => {
  const errorMessage = () => () => {
    const pattern = /^[a-zA-Z\s]+$/;
    if (text) {
      if (pattern.test(text)) return false;
    }
    if (options) {
      if (options.errorMessage) {
        return options.errorMessage;
      }
    }
    return 'This field may only contain alphabetic characters as well as spaces';
  };

  return errorMessage();
};

export default alphaSpace;
