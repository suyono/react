const maxLength = options => text => {
  const errorMessage = () => () => {
    if (options.length) {
      if (text.length > options.length) {
        if (options.errorMessage) {
          return options.errorMessage;
        }
        return `This field may not be greater than ${options.length} characters`;
      }
    }
    return false;
  };

  return errorMessage();
};

export default maxLength;
