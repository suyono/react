const geolocation = options => text => {
  const errorMessage = () => () => {
    let pattern;
    if(options.type === 'latitude'){
      pattern = /^-{0,1}((90|90.[0]{1,20}|[0-9]|[1-8][0-9])|(89|[0-9]|[1-8][0-9])[.]{1}[0-9]{1,20}){1}$/;
    }else if(options.type === 'longitude'){
      pattern = /^-{0,1}((180|180.[0]{1,20}|[0-9]|([0-9][0-9])|([1][0-7][0-9]))|(179|[0-9]|([0-9][0-9])|([1][0-7][0-9]))[.]{1}[0-9]{1,20}){1}$/;
    }
    if (text) {
      if (pattern.test(text)) return false;
    }
    if (options) {
      if (options.errorMessage) {
        return options.errorMessage;
      }
    }
    return 'Invalid data';
  };

  return errorMessage();
};

export default geolocation;
