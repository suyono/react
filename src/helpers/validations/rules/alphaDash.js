const alphaDash = options => text => {
  const errorMessage = () => () => {
    const pattern = /^[a-zA-Z0-9-_]+$/;
    if (text) {
      if (pattern.test(text)) return false;
    }
    if (options) {
      if (options.errorMessage) {
        return options.errorMessage;
      }
    }
    return 'This field may contain alphabetic characters, numbers, dashes or underscores';
  };

  return errorMessage();
};

export default alphaDash;
