import array from './array';
import captcha from './captcha';
import convert from './convert';
import pagination from './pagination';
import urlQuery from './urlQuery';
import validation from './validation';
import validations from './validations';
import authentication from './authentication';

const Helpers={};
Helpers.Array = array;
Helpers.Captcha = captcha;
Helpers.Convert = convert;
Helpers.Pagination = pagination;
Helpers.UrlQuery = urlQuery;
Helpers.Validation = validation;
Helpers.Validations = validations;
Helpers.Authentication = authentication;

export default Helpers;
