﻿# List of Component React UI

- [List of Component React UI](#list-of-component-react-ui)
  - [Alert](#alert)
  - [Breadcrumb](#breadcrumb)
  - [Button](#button)
  - [Card](#card)
  - [Column](#column)
  - [Container](#container)
  - [Content Loader](#content-loader)
  - [Datepicker](#datepicker)
  - [Footer](#footer)
  - [Header](#header)
  - [Icon Fontawesome](#icon-fontawesome)
  - [Image](#image)
    - [Image Popup](#image-popup)
  - [Input](#input)
    - [Input Password](#input-password)
    - [Input AlphaNumeric](#input-alphanumeric)
    - [Input Autocomplete](#input-autocomplete)
    - [Input Code](#input-code)
    - [Input Decimal](#input-decimal)
    - [Input Email](#input-email)
    - [Input File](#input-file)
    - [Input Number](#input-number)
    - [Input NumberFormat](#input-numberformat)
    - [Input Richtext](#input-richtext)
    - [Input SpecialChar](#input-specialchar)
    - [Input Textarea](#input-textarea)
    - [Input Checkbox](#input-checkbox)
    - [Input Radiobutton](#input-radiobutton)
  - [Layout](#layout)
  - [Loader](#loader)
  - [LoadIndicator](#loadindicator)
  - [Message Success](#message-success)
  - [Modal](#modal)
  - [Confirmation Modal](#confirmation-modal)
  - [Navigation Bar](#navigation-bar)
  - [Pagination](#pagination)
  - [Select](#select)
  - [SelectBox](#selectbox)
  - [SelectSearch](#selectsearch)
  - [Switch](#switch)
  - [Table](#table)
  - [Table with Select](#table-with-select)
  - [Menu Tabs](#menu-tabs)
  - [Text](#text)
  - [Title](#title)
  - [Tree](#tree)

## Alert
Code untuk menambahkan **Alert** :
```javascript
import { Alert } from  'component-ui-web-teravin';
```
Contoh penggunaan alert error dan alert warning :
```javacript
Alert.error({message: 'Oops, data is alraedy exist'});
Alert.warning({message: 'User not registered!'});
```
Custom props:

| Name      | Type   | Default  | Description                        |
| --------- | ------ | -------- | ---------------------------------- |
| message   | string | -        | Pesan error                        |
| autoClose | bool   | true     | Alert otomatis tertutup atau tidak |
| duration  | number | 5000     | Durasi tampil alert dalam second   |
| position  | string | 'center' | Posisi alert                       |

## Breadcrumb
Komponen breadcrumb digunakan untuk menunjukkan urutan menu yang sedang dibuka. Code untuk menambahkan **Breadcrumb** :
```javascript
import { Breadcrumb } from 'component-ui-web-teravin';
```
Contoh penggunaan Breadcrumb :
```javascript
<Breadcrumb location={breadcrumb} />
```
Custom props :

| Name     | Type              | Default | Description      |
|----------|-------------------|---------|------------------|
| location | object (required) | -       | Location history |

## Button
Komponen button terdiri dari 2 jenis, yaitu button itu sendiri dan button datepicker. Button datepicker digunakan untuk custom input tanggal berbentuk button, yang artinya tanggal tidak dapat diinput dengan diketik.

Code untuk menambahkan **Button** :
```javascript
import { Button } from  'component-ui-web-teravin';
```
Contoh penggunaan Button :
```javascript
<Button
  role="submit"
  size="medium"
  loading={isLogin}
  block
  disabled={disabledLogin}
  type="info"
>Login</Button>
```
Custom props :

| Name        | Type                                                                                 | Default   | Description                                                                                    |
| ----------- | ------------------------------------------------------------------------------------ | --------- | ---------------------------------------------------------------------------------------------- |
| outlined    | bool                                                                                 | false     | Jika true, Border berwarna dan background putih                                                |
| inverted    | bool                                                                                 | false     | Jika true, warna teks menjadi warna latar, dan sebaliknya                                      |
| loading     | bool                                                                                 | false     | Jika true, teks menjadi icon loading                                                           |
| disabled    | bool                                                                                 | false     | Jika true, button disable                                                                      |
| block       | bool                                                                                 | false     | Jika true, panjang button full dengan bodynya                                                  |
| display     | string                                                                               | -         | Style css display                                                                              |
| size        | string ('small'/'medium'/'large'/'normal')                                           | -         | Ukuran lebar button                                                                            |
| role        | string ('button'/'submit'/'reset')                                                   | 'button'  | Type button                                                                                    |
| type        | string ('white'/'light'/'dark'/'link'/'primary'/'info'/'success'/'warning'/'danger') | 'primary' | Warna button, contoh dapat dilihat di [Bulma](https://bulma.io/documentation/elements/button/) |
| onClick     | function                                                                             | (){}      | Fungsi ketika klik button                                                                      |
| index       | string                                                                               | -         | Id button                                                                                      |
| fontColor   | string                                                                               | 'white'   | Warna teks pada button                                                                         |
| customStyle | object                                                                               | -         | Digunakan untuk meletakkan custom style button                                                 |



Untuk penggunaan **Button Datepicker**, dipanggil dalam props `customInput` di komponen **Datepicker**:
```javascript
<Button.Datepicker  error={errors.dateFrom}  width={220} />
```
Custom props :

| Name     | Type                                                                                 | Default   | Description                                                                                    |
| -------- | ------------------------------------------------------------------------------------ | --------- | ---------------------------------------------------------------------------------------------- |
| value    | any                                                                                  | -         | Teks label button                                                                              |
| id       | any                                                                                  | -         | Id Button                                                                                      |
| outlined | bool                                                                                 | false     | Jika true, Border berwarna dan background putih                                                |
| loading  | bool                                                                                 | false     | Jika true, teks menjadi icon loading                                                           |
| disabled | bool                                                                                 | false     | Jika true, button disable                                                                      |
| block    | bool                                                                                 | false     | Jika true, panjang button full dengan bodynya                                                  |
| size     | string ('small'/'medium'/'large'/'normal')                                           | -         | Ukuran lebar button                                                                            |
| type     | string ('white'/'light'/'dark'/'link'/'primary'/'info'/'success'/'warning'/'danger') | 'primary' | Warna button, contoh dapat dilihat di [Bulma](https://bulma.io/documentation/elements/button/) |
| error    | string                                                                               | -         | Indikator error input                                                                          |
| width    | string / number                                                                      | 250       | Panjang button                                                                                 |

## Card
Digunakan untuk membuat tampilan seperti kartu. Di project Teravin biasanya digunakan untuk kolom filter di menu list.
Card terdiri dari 5 bagian yaitu, **Card** sebagai pembuka, **Card Header** bagian header, **Card Content** bagian content berupa text, **Card Image** bagian content berupa gambar dan **Card Footer** bagian footer.
Code untuk menambahkan **Card** :
```javascript
import { Card } from 'component-ui-web-teravin';
```
Contoh penggunaan **Card** untuk kolom filter dalam screen List:
```javascript
<Card>
  <Card.Header title="Filter" />
  <Card.Content>
    <div className="content">
      <form onSubmit={this.onSubmitFilter}>
        ...
      </form>
    </div>
  </Card.Content>
  <Card.Footer>
    <Button>Reset</Button>
  </Card.Footer>
</Card>
```
Custom props **Card** :

| Name       | Type   | Default | Description            |
|------------|--------|---------|------------------------|
| background | string | -       | Untuk warna background |
| color      | string | -       | Untuk warna teks       |

Custom props **Card Header** :

| Name   | Type              | Default | Description                |
|--------|-------------------|---------|----------------------------|
| center | bool              | true    | Untuk mengatur rata teks   |
| title  | string (required) | ''      | Untuk teks judul           |
| color  | string            | -       | Untuk warna teks           |
| size   | number / string   | 1.5rem  | Untuk mengatur ukuran teks |

Custom props **Card Content** :
Props Card Content hanya berisi children

Custom props **Card Image** :

| Name     | Type              | Default | Description      |
|----------|-------------------|---------|------------------|
| urlImage | string (required) |         | Untuk url gambar |

Custom props **Card Footer** :

| Name  | Type   | Default | Description        |
|-------|--------|---------|--------------------|
| style | object | -       | Untuk style footer |

## Column
Digunakan untuk membuat kolom yang berisi nama dan value, misal dalam screen Add Form. Dalam screen Add Form akan terdiri dari beberapa field yg berisi nama dan inputan/value.
Column terdiri dari 4 bagian yaitu, **Column** sebagai pembuka, **Column Content** sebagai pembuka di tiap baris, **Column Name untuk** menuliskan label/nama, dan **Column Value** untuk menuliskan value.
Code untuk menambahkan **Column**:
```javascript
import {Column} from 'component-ui-web-teravin';
```
Contoh penggunaan **Column** dalam screen Add Form:
```javascript
<Column>
  <Column.Content>
    <Column.Name text="Code" />
    <Column.Value>
      <Input ... />
    </Column.Value>
  </Column.Content>
  <Column.Content>
    <Column.Name text="Name" />
    <Column.Value>Teravin</Column.Value>
  </Column.Content>
</Column>
```
Custom props **Column**:

| Name    | Type   | Default | Description               |
| ------- | ------ | ------- | ------------------------- |
| padding | number | 14      | Untuk style padding kolom |

Custom props **Column Content**:

| Name         | Type            | Default             | Description                                         |
| ------------ | --------------- | ------------------- | --------------------------------------------------- |
| borderBottom | string          | '1px solid #efefef' | Style ketebalan dan warna garis bawah tiap baris    |
| isBorder     | bool            | true                | Jika true, Untuk menambahkan garis bawah tiap baris |
| id           | string / number | -                   | Id tiap baris                                       |
| style        | object          | {}                  | tambahan style untuk baris                          |

Custom props **Column Name**:

| Name     | Type            | Default | Description                             |
| -------- | --------------- | ------- | --------------------------------------- |
| text     | string          | -       | Untuk menambahkan teks/keterangan field |
| padding  | string / number | 14      | Style padding untuk kolom nama field    |
| width    | string / number | 210     | Lebar kolom nama field                  |
| bold     | bool            | true    | style tebal untuk teks                  |
| fontSize | number          | 13      | Ukuran font teks                        |
| colSpan  | string          | -       | Untuk merge column                      |

Custom props **Column Value**:

| Name        | Type            | Default | Description                          |
| ----------- | --------------- | ------- | ------------------------------------ |
| padding     | string / number | 14      | Style padding untuk kolom nama field |
| customStyle | object          | {}      | Untuk custom style column            |

## Container
Digunakan untuk mengawali conten dalam sebuah screen.
Code untuk menambahkan **Container** :
```javascript
import { Container } from 'component-ui-web-teravin';
```
Contoh penggunaan :
```javascript
<Container fluid>
...
</Container>
```
Custom props :

| Name  | Type   | Default | Description                                                                        |
|-------|--------|---------|------------------------------------------------------------------------------------|
| fluid | bool   | false   | Untuk menambahkan style 'is-fluid' (menjaga margin tetap 32px pada kiri dan kanan) |
| style | object | -       | Untuk custom style                                                                 |

## Content Loader
Digunakan untuk memberi effect loading page, custom effect bisa di buat melalui https://danilowoz.com/create-content-loader/ . Untuk default bisa gunakan loader detail yang sudah dibuat berupa list 2 column.
Code untuk menambahkan **Content Loader** :
```javascript
import {ContentLoader} from 'component-ui-web-teravin';
```
Contoh penggunaan Content Loader dengan type detail :
```javascript
<ContentLoader.Detail />
```

## Datepicker
Digunakan untuk inputan tanggal. datepicker ini menggunakan library `react-datepicker` yang dapat dilihat dokumentasinya di https://reactdatepicker.com/.
Code untuk menambahkan **Datepicker**:
```javascript
import {DatePicker} from 'component-ui-web-teravin';
```
Untuk menggunakan Datepicker, kita juga harus menambahkan library **moment** untuk converter tanggal, untuk dokumentasi moment itu sendiri dapat dilihat di https://momentjs.com/docs/.
Contoh penggunaan **Datepicker**:
```javascript
import moment from 'moment';
...
onChangeDate(data){
  let date, errorDate;
  const {value, name, isValid, rawValue} = data;
  if(isValid){
    date = value;
  }else{
    if(rawValue == ''){
      errorDate = '';
    }else{
      errorDate = 'Invalid Date';
    }
  }
  this.setState({
    date: date,
    errorDate: errorDate
  });
}
render(){
  const {date, errorDate} = this.state;
  return(
	<DatePicker
	  customInput={<Button.Datepicker .../>}
	  name="dateFrom"
	  value={date}
	  onChange={this.onChangeDate}
	  error={errorDate}
	  dateFormat="DD/MM/YYYY HH:mm"
	/>
  );
}
...
```
Custom props **Datepicker**:

| Name              | Type                                       | Default      | Description                                                                                                                                  |
| ----------------- | ------------------------------------------ | ------------ | -------------------------------------------------------------------------------------------------------------------------------------------- |
| value             | object                                     | -            | Value dari datepicker, berupa object `moment`                                                                                                |
| name              | string                                     | -            | Nama dari element datepicker                                                                                                                 |
| error             | string                                     | -            | Indikator error input                                                                                                                        |
| dateFormat        | string                                     | 'DD/MM/YYYY' | Format date yang akan ditampilkan. Dokumentasi untuk date format dapat dilihat [di sini](https://momentjs.com/docs/#/parsing/string-format/) |
| onChange          | function (required)                        | onChange(){} | Fungsi untuk mengolah data input tanggal                                                                                                     |
| size              | string ('small'/'medium'/'large'/'normal') | 'small'      | Ukuran lebar Datepicker                                                                                                                      |
| disabled          | bool                                       | false        | Untuk disable Datepicker                                                                                                                     |
| width             | number                                     | 220          | Ukuran panjang Datepicker                                                                                                                    |
| interval          | number                                     | -            | Untuk time interval jika Datepicker menggunakan time                                                                                         |
| id                | string / number                            | -            | Id Datepicker                                                                                                                                |
| showTimeSelect    | bool                                       | -            | Jika true, maka menampilkan time(waktu) dalam inputan datepicker                                                                             |
| showMonthDropdown | bool                                       | true         | Menampilkan dropdown pilihan bulan                                                                                                           |
| showYearDropdown  | bool                                       | true         | Menampilkan dropdown pilihan tahun                                                                                                           |
| isClearable       | bool                                       | true         | Menampilkan button clear (x) di ujung inputan untuk mereset datepicker                                                                       |
| minRealTime       | bool                                       | false        | Untuk pembatasan waktu (jika menampilkan time) berdasar waktu saat ini                                                                       |
| customInput       | element                                    | -            | Untuk menampilkan custom input berupa button atau yang lainnya                                                                               |

## Footer
Digunakan untuk membuat tampilan footer.
Code untuk menambahkan **Footer** :
```javascript
import {Footer} from 'component-ui-web-teravin';
```
Contoh penggunaan footer:
```javascript
<Footer lineColor="blue" lineWidth="100%" />
```
Custom props :

| Name            | Type            | Default | Description                                |
|-----------------|-----------------|---------|--------------------------------------------|
| logo            | string          | -       | Untuk url image logo                       |
| nameText        | string          | -       | Untuk custom teks nama pada footer         |
| csText          | string          | -       | Untuk custom deskripsi teks pada footer    |
| backgroundColor | string          | white   | Untuk custom warna background              |
| lineWidth       | string / number | '90%'   | Untuk ukuran panjang garis                 |
| lineColor       | string          | -       | Untuk warna garis                          |
| styleLogo       | object          | -       | Untuk custom style pada gambar logo        |
| back            | string          | -       | Untuk custom background gambar pada footer |

## Header
Digunakan untuk membuat tampilan header.
Code untuk menambahkan :
```javascript
import { Header } from 'component-ui-web-teravin';
```
Contoh penggunaan :
```javascript
<Header title={title}/>
```
Custom props :

| Name        | Type              | Default | Description                    |
|-------------|-------------------|---------|--------------------------------|
| title       | string (required) | 'Title' | Untuk teks judul               |
| center      | bool              | true    | Untuk rata teks judul          |
| textColor   | string            | -       | Untuk warna teks               |
| customStyle | object            | -       | Untuk custom style pada header |

## Icon Fontawesome
Digunakan untuk menampilkan icon yang diambil dari Icon Fontawesome. Untuk mengambil kode icon dapat dilihat [di sini](https://fontawesome.com/icons?d=gallery&m=free).
Code untuk menambahkan **Icon Fontawesome**:
```javascript
import {Icon} from 'component-ui-web-teravin';
```
Contoh penggunaan untuk menampilkan icon `logout`:
```javascript
<Icon iconName="fa-sign-out-alt" size="3x"/>
```
Custom props **Icon Fontawesome**:

| Name     | Type                                          | Default | Description                                                                                                           |
| -------- | --------------------------------------------- | ------- | --------------------------------------------------------------------------------------------------------------------- |
| color    | string ('blue'/'dark'/'green'/'orange'/'red') | 'dark'  | Warna icon                                                                                                            |
| size     | string                                        | -       | Ukuran icon, dokumentasi dapat dilihat [di sini]( https://fontawesome.com/how-to-use/on-the-web/styling/sizing-icons) |
| iconName | string (required)                             | -       | Nama icon yang sesuai dengan Font Awesome                                                                             |
| style    | object                                        | {}      | Untuk custom style jika perlu                                                                                         |
| position | string ('left'/'right')                       | -       | Untuk menentukan posisi icon                                                                                          |
| onClick  | function                                      | (){}    | Menambah action klik pada icon                                                                                        |

## Image
Digunakan untuk menampilkan gambar. **Image** memiliki 2 jenis tampilan yaitu **Image** itu sendiri menampilkan gambar biasa dan [**Image Popup**](#image-popup) untuk menampilkan gambar dalam bentuk popup.
Code untuk menambahkan **Image**:
```javascript
import {Image} from 'component-ui-web-teravin';
```
Contoh penggunaan **Image**:
```javascript
<Image
  src={https://i5.walmartimages.ca/images/Enlarge/062/0_r/6000191270620_R.jpg}
  alt="Apple"
/>
```
Custom props **Image**:

| Name   | Type            | Default                                          | Description          |
| ------ | --------------- | ------------------------------------------------ | -------------------- |
| src    | any             | require ( '../../assets/img/default-image.jpg' ) | Url image            |
| alt    | string          | -                                                | Alt image            |
| width  | string / number | 100                                              | Ukuran panjang image |
| height | string / number | auto                                             | Ukuran lebar image   |

### Image Popup
Contoh penggunaan **Image Popup**:
```javascript
...
onCloseImage(){
  this.setState({showImage: false});
}
...
render(){
  const {showImage} = this.state;
  return(
	<Image.Popup
	  active={showImage}
	  src={https://i5.walmartimages.ca/images/Enlarge/062/0_r/6000191270620_R.jpg}
	  onClose={this.onCloseImage}
	/>
  )
}
...
```
Custom Props **Image Popup**:

| Name    | Type            | Default                                          | Description                      |
| ------- | --------------- | ------------------------------------------------ | -------------------------------- |
| src     | any             | require ( '../../assets/img/default-image.jpg' ) | Url image                        |
| active  | bool            | -                                                | Untuk menampilkan popup image    |
| width   | string / number | 100                                              | Ukuran panjang image             |
| height  | string / number | auto                                             | Ukuran lebar image               |
| onClose | function        | -                                                | Fungsi untuk menutup popup image |

## Input
Digunakan untuk membuat inputan teks dengan menggunakan tag html `<input>`. Input terdiri dari 10 jenis inputan yaitu **Input**, [**Input Password**](#input-password), [**Input AlphaNumeric**](#input-alphanumeric), [**Input Autocomplete**](#input-autocomplete), [**Input Code**](#input-code), [**Input Decimal**](#input-decimal), [**Input File**](#input-file), [**Input Textarea**](#input-textarea), [**Input Checkbox**](#input-checkbox), [**Input Radiobutton**](#input-radiobutton).
Code untuk menambahkan **Input**:
```javascript
import {Input} from 'component-ui-web-teravin';
```
Contoh penggunaan **Input**:
```javascript
...
onChangeName(data){
  let {name, errorName} = this.state;
  const {value} = data;
  if(value){
    name = value;
    errorName = '';
  else{
    errorName = 'This field is required';
  }
  this.setState({name: name, errorName: errorName});
}
...
render(){
  const {name, errorName} = this.state;
  return(
	<Input
	  name="name"
	  value={name}
	  error={errorName}
	  onChange={this.onChangeName}
	  width={300}
	  maxLength={40}
	/>
  )
}
...
```
Custom props **Input**:

| Name         | Type                                       | Default      | Description                                                                          |
| ------------ | ------------------------------------------ | ------------ | ------------------------------------------------------------------------------------ |
| value        | string                                     | ''           | Value yang diinput                                                                   |
| placeholder  | string                                     | -            | Teks placeholder dalam kolom input                                                   |
| name         | string                                     | -            | Nama kolom input                                                                     |
| error        | string                                     | -            | Indikasi error input                                                                 |
| maxLength    | number                                     | 50           | Maksimal panjang karakter yang diinput                                               |
| onChange     | function (required)                        | onChange(){} | Fungsi untuk mengolah data inputan                                                   |
| size         | string ('small'/'medium'/'large'/'normal') | 'small'      | Ukuran lebar kolom input                                                             |
| loading      | bool                                       | -            | Jika true, akan ada icon loading di sebalah kanan kolom input                        |
| disabled     | bool                                       | -            | Jika true, input disable                                                             |
| hasAddon     | bool                                       | -            | Jika true, dapat menambahkan element icon di kolom input                             |
| width        | number                                     | -            | Ukuran panjang kolom input                                                           |
| style        | object                                     | {}           | Custom style untuk kolom input                                                       |
| language     | object                                     | {}           | Untuk menambahkan message berdasar language (jika aplikasi mendukung multi language) |
| autocomplete | string                                     | 'off'        | Jika on, autocomplete input di browser aktif, dan sebaliknya                         |
| hasIconLeft  | element                                    | -            | Menambah icon di dalam field input sebelah kiri                                      |
| hasIconRight | element                                    | -            | Menambah icon di dalam field input sebelah kanan                                     |
| styleInput   | object                                     | {}           | Untuk menambah custom style input                                                    |

### Input Password
Digunakan untuk membuat inputan password dengan teks yang tersembunyi.
Contoh penggunaan **Input Password**:
```javascript
...
onChangePassword(data){
  let {password, errorPassword} = this.state;
  const {value} = data;
  if(value){
    password = value;
    errorPassword = '';
  else{
    errorPassword = 'This field is required';
  }
  this.setState({password: password, errorPassword: errorPassword});
}
...
render(){
  const {password, errorPassword} = this.state;
  return(
	<Input.Login
	  name="password"
	  value={password}
	  error={errorPassword}
	  onChange={this.onChangePassword}
	  width={300}
	/>
  )
}
...
```
Custom props input password sama dengan custom props pada [**Input**](#input).

### Input AlphaNumeric
Digunakan untuk membuat inputan hanya berupa alphabet a-z / A-Z, numeric 0-9 dan spasi.
Contoh penggunaan **Input AlphaNumeric**:
```javascript
...
onChangeAlphanumeric(data){
  let {alphanumeric, errorAlphanumeric} = this.state;
  const {value} = data;
  if(value){
    alphanumeric = value;
    errorAlphanumeric = '';
  else{
    errorAlphanumeric = 'This field is required';
  }
  this.setState({alphanumeric: alphanumeric, errorAlphanumeric: errorAlphanumeric});
}
...
render(){
  const {alphanumeric, errorAlphanumeric} = this.state;
  return(
	<Input.Alphanumeric
	  name="alphanumeric"
	  value={alphanumeric}
	  error={errorAlphanumeric}
	  onChange={this.onChangeAlphanumeric}
	/>
  )
}
...
```
Custom props untuk input alphanumeric ini sama dengan custom props [**Input**](#input).

### Input Autocomplete
Digunakan untuk membuat inputan auto complete.
Contoh penggunaan :
```javascript
<Input.Autocomplete
  name="name"
  value={name}
  onChange={this.onChanges}
  onSelectSuggestion={this.onSelect}
  width={300}
  suggestions={arrayList}
  minCharSuggestion={3}
  onSuggestion={true}
/>
```
Custom props input autocomplete sama dengan custom props pada [**Input**](#input) dengan tambahan props sebagai berikut:

| Name               | Type                | Default                 | Description                                             |
|--------------------|---------------------|-------------------------|---------------------------------------------------------|
| suggestions        | array               | []                      | Untuk array data suggestion                             |
| minCharSuggestion  | number              | 1                       | Untuk minimal character input suggeston                 |
| onSelectSuggestion | function (required) | onSelectSuggestion (){} | Untuk function ketika suggestion dipilih                |
| openSuggestion     | bool                | false                   | Untuk indikasi suggestion muncul atau hidden            |
| auto               | bool                | true                    | Untuk indikasi apakah suggestion auto muncul atau tidak |

### Input Code
Digunakan untuk inputan spesial ignore character / . % # ~ ; ? \ |. biasanya digunakan untuk form input code.
Contoh penggunaan :
```javascript
<Input.InputCode
  name="code"
  value={form.code}
  onChange={this.onChangeForm}
  width={300}
  maxLength={5}
/>
```
Custom props input code sama dengan custom props pada [**Input**](#input)

### Input Decimal
Digunakan untuk inputan berbentuk decimal.
Contoh penggunaan untuk inputan (5,2 = 3 bilangan bulat dan 2 bilangan di belakang koma) :
```javascript
...
isAllowed(data){
  const {value} = data;
    var arrValue = value.split('.');
    if(arrValue[0].length > 3){
      return false;
    }else{
      return true
    }
}
...
render(){
  return(
    <Input.Decimal
      name="weight"
      value={weight}
      onChange={this.onChangeForm}
      decimalScale={2}
      fixedDecimalScale={false}
      isAllowed={this.isAllowed}
    />
  );
}
...
```
Custom props input decimal sama dengan custom props pada [**Input**](#input) dengan tambahan props sebagai berikut:

| Name              | Type   | Default | Description                                                                                 |
|-------------------|--------|---------|---------------------------------------------------------------------------------------------|
| decimalSeparator  | string | -       | Untuk character pemisah bilangan bulat dan bilangan decimal. berisi titik (.) atau koma (,) |
| decimalScale      | number | -       | Untuk banyaknya digit decimal                                                               |
| fixedDecimalScale | bool   | true    | Untuk indikasi apakah decimal tetap ada walau bernilai 0 atau tidak                         |
| allowNegative     | bool   | -       | Untuk indikasi bilangan negatif                                                             |
| isAllowed         | func   | -       | Fungsi untuk mengatur bilangan yang diizinkan                                               |

### Input Email
Digunakan untuk inputan dengan validasi format email.
Contoh penggunaan :
```javascript
<Input.Email
  name="email"
  value={email}
  error={error}
  width={300}
  onChange={this.onChangeForm}
/>
```
Custom props input email sama dengan custom props pada [**Input**](#input)

### Input File
Digunakan untuk membuat inputan yang mengambil file dari local disk.
Contoh penggunaan **Input File**:
```javascript
...
onChangeImportFile(data, files){
  const {form, errors} =  this.state;
  const {id} =  data;
  if(files){
    const  value  =  files;
    const  name  = value.name;
    const  extension  = name.substring(name.lastIndexOf('.')+1);
    if(extension  ==  'xls'  ||  extension  ==  'xlsx'){
      form.file  =  value;
      errors.file  =  '';
      this.setState({
        importFile: name,
        form: form,
        errors: errors,
        isDisableUpload: false
      });
    }else{
      errors.file  =  'This type of file not allow';
      form.file  =  null;
      document.getElementById(id).value  =  null;
      this.setState({
        form: form,
        errors: errors,
        importFile: 'No file selected',
        isDisableUpload: true
      });
    }
  }
}
...
render(){
  const {importFile, errors} = this.state;
  return(
	<Input.File
	  name="importFile"
	  id="importFile"
	  acceptTypeFile="application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
	  onChange={this.onChangeImportFile}
	  filename={importFile}
	  error={errors.file}
	/>
  )
}
...
```
Custom props **Input File**:

| Name           | Type                                                                                         | Default            | Description                                                                          |
| -------------- | -------------------------------------------------------------------------------------------- | ------------------ | ------------------------------------------------------------------------------------ |
| onChange       | function                                                                                     | onChange(){}       | Fungsi yang digunakan untuk mengolah data file                                       |
| size           | string ('small'/'medium'/'large'/'normal')                                                   | 'small'            | Ukuran lebar kolom input file                                                        |
| color          | string ('white'/'light'/'black'/'dark'/'link'/'primary'/'info'/'success'/'warning'/'danger') | 'primary'          | Warna button kolom input file                                                        |
| acceptTypeFile | string                                                                                       | -                  | Tipe file yang diterima oleh inputan                                                 |
| name           | string                                                                                       | -                  | Nama kolom input file                                                                |
| filename       | string                                                                                       | 'No selected file' | Deklarasi nama file yang diinput                                                     |
| id             | any                                                                                          | 'file-upload'      | Id kolom input file                                                                  |
| error          | string                                                                                       | -                  | Indikasi untuk error input file                                                      |
| language       | object                                                                                       | {}                 | Untuk menambahkan message berdasar language (jika aplikasi mendukung multi language) |
| disabled       | bool                                                                                         | -                  | Indikasi disabled input file                                                         |
| multiple       | bool                                                                                         | false              | Untuk memilih file lebih dari 1                                                      |
| base64         | bool                                                                                         | false              | Untuk convert file ke format base64                                                  |

### Input Number
Digunakan untuk inputan dengan validasi hanya angka saja.
Contoh penggunaan :
```javascript
<Input.Number
  width={300}
  name="phoneNo"
  value={phoneNo}
  error={error}
  onChange={this.onChangeForm}
  maxLength={13}
/>
```
Custom props input number sama dengan custom props pada [**Input**](#input)

### Input NumberFormat
Digunakan untuk inputan berupa nominal uang.
Contoh penggunaan :
```javascript
<Input.NumberFormat
  name="sumInsured"
  value={sumInsured}
  error={errors}
  onChange={this.onChangeForm}
  decimalScale={2}
  isAllowed={this.isAllowed}
  textAlign="right"
/>
```
Custom props input numberFormat sama dengan custom props pada [**Input**](#input) dengan tambahan props sebagai berikut:

| Name              | Type          | Default | Description                                                                                            |
|-------------------|---------------|---------|--------------------------------------------------------------------------------------------------------|
| thousandSeparator | string / bool | true    | Jika true maka thousand separator berupa tanda koma (,). jika ingin selain koma maka isi dengan string |
| decimalSeparator  | string        | .       | Untuk character pemisah bilangan bulat dan bilangan decimal. berisi titik (.) atau koma (,)            |
| decimalScale      | number        | -       | Untuk banyaknya digit decimal                                                                          |
| fixedDecimalScale | bool          | true    | Untuk indikasi apakah decimal tetap ada walau bernilai 0 atau tidak                                    |
| allowNegative     | bool          | -       | Untuk indikasi bilangan negatif                                                                        |
| isAllowed         | func          | -       | Fungsi untuk mengatur bilangan yang diizinkan                                                          |
| hasSymbol         | bool          | -       | Untuk indikasi tampilan simbol terpisah                                                                |
| currency          | string        | -       | Untuk lambang mata uang jika hasSymbol adalah true                                                     |
| hasText           | bool          | true    | Untuk menampilkan teks deskripsi di bawah kolom inputan berupa "This field contains only numbers"      |
| prefix            | string        | -       | Untuk tambahan karakter sebelum inputan angka. biasanya berupa lambang currency                        |
| suffix            | string        | -       | Untuk tambahan karakter di akhir setelah inputan angka                                                 |
| textAlign         | string        | -       | Untuk rata teks dalam kolom                                                                            |

### Input Richtext
Digunakan untuk tampilan inputan dengan banyak konfigurasi yang dapat dilakukan oleh user, seperti rata teks, ukuran teks, style teks.
Contoh penggunaan:
```javascript
<Input.RichText
  onChange={this.onChangeRichText}
  readOnly={false}
  value={valueRichText}
  error={errors}
/>
```
### Input SpecialChar

### Input Textarea
Digunakan untuk membuat inputan berupa textarea.
Contoh penggunaan **Input Textarea**:
```javascript
...
onChangeAddress(data){
  let {address, errorAddress} = this.state;
  const {value} = data;
  if(value){
    address = value;
    errorAddress = '';
  else{
    errorAddress = 'This field is required';
  }
  this.setState({address: address, errorAddress: errorAddress});
}
...
render(){
  return(
	<Input.Textarea
	  name="address"
	  value={address}
	  onChange={this.onChangeAddress}
	  error={errorAddress}
	/>
  )
}
...
```
Custom props **Input Textarea**:

| Name        | Type                                       | Default      | Description                                 |
| ----------- | ------------------------------------------ | ------------ | ------------------------------------------- |
| value       | string                                     | ''           | value yang diinput                          |
| id          | string / number                            | -            | Id field textarea                           |
| size        | string ('small'/'medium'/'large'/'normal') | 'small'      | Ukuran lebar kolom input textarea           |
| placeholder | string                                     | -            | Teks placeholder dalam kolom input textarea |
| rows        | string                                     | ''           | Jumlah baris default yang tampil            |
| name        | string                                     | -            | Nama kolom input file                       |
| maxLength   | number                                     | 255          | Maksimal karakter yang dapat diinput        |
| onChange    | function                                   | onChange(){} | Fungsi untuk mengolah data inputan          |
| loading     | bool                                       | -            | Indikasi loading                            |
| disabled    | bool                                       | -            | Jika true, input textarea disable           |
| width       | number                                     | -            | Ukuran panjang kolom input textarea         |

### Input Checkbox
Digunakan untuk membuat input multi pilihan berupa checkbox.
Contoh penggunaan **Input Checkbox**:
```javascript
...
this.state={
  interests: []
}
...
onChange(data){
  let {interests} = this.state;
  const {id, checked} = data;
  if(checked){
    interests.push(id);
  }else{
    if(interests.includes(id)){
      const index = interests.indexOf(id);
      interests.splice(index, 1);
    }
  }

  this.setState({interests: interests});
}
...
render(){
  const {interests} = this.state;
  return(
    <div>
      <Input.Checkbox
        id="singing"
	name="interests"
	checked={interests.includes('singing') ?  true  :  false}
	text="Singing"
	onChange={this.onChange}
      />
      <Input.Checkbox
        id="swimming"
        name="interests"
        checked={interests.includes('swimming') ? true : false}
        text="Swimming"
        onChange={this.onChange}
      />
      <Input.Checkbox
        id="photography"
        name="interests"
        checked={interests.includes('photography') ? true : false}
        text="Photography"
        onChange={this.onChange}
      />
    </div>
  )
}
...
```
Custom props **Input Checkbox**:

| Name     | Type                | Default      | Description                             |
| -------- | ------------------- | ------------ | --------------------------------------- |
| value    | string              | ''           | Value dari inputan                      |
| name     | string              | -            | Nama dari kolom input checkbox          |
| onChange | function (required) | onChange(){} | Fungsi untuk mengolah data inputan      |
| disabled | bool                | -            | Jika true, input checkbox disable       |
| width    | number              |              | Ukuran panjang kolom input checkbox     |
| style    | object              | {}           | Custom style untuk kolom input checkbox |
| text     | string              | ''           | Label input checkbox                    |
| checked  | bool                | -            | Jika true, input checkbox terpilih      |

### Input Radiobutton
Digunakan untuk membuat input pilihan berupa radiobutton.
Contog penggunaan **Input Radiobutton**:
```javascript
...
onChange(data){
  let {gender} = this.state;
  const {id} = data;
  gender = id;
  this.setState({gender: gender});
}
...
render(){
  const {gender} = this.state;
  return(
    <div>
      <Input.Radiobutton
        id="L"
	name="gender"
	value={gender}
	checked={gender ==  'L'  ?  true  :  false}
	text="Laki-laki"
	onChange={this.onChange}
      />
      <Input.Radiobutton
        id="P"
        name="gender"
        value={gender}
        checked={gender == 'P' ? true : false}
        text="Perempuan"
        onChange={this.onChange}
    </div>
  )
}
...
```
Custom props **Input Radiobutton**:
Custom props untuk input radiobutton ini sama dengan custom props pada [**Input Checkbox**](#input_checkbox)

## Layout

## Loader

## LoadIndicator

## Message Success
Digunakan untuk menampilkan pemberitahuan aksi sukses berupa toast message. Selain aksi sukses, message ini juga dapat ditampilkan sebagai pesan `info`, `warning`, dan juga `danger`, tergantung dari `type` yang dipilih.
Code untuk menambahkan **Message Success**:
```javascript
import {Message} from 'component-ui-web-teravin';
```
Contoh penggunaan **Message Success**:
```javascript
<Message
  show={true}
  message="Success save data"
/>
```
Custom props **Message Success**:

| Name      | Type                                            | Default   | Description                                                       |
| --------- | ----------------------------------------------- | --------- | ----------------------------------------------------------------- |
| show      | bool                                            | false     | Indikator untuk menampilkan message                               |
| message   | string                                          | -         | Pesan yang akan ditampilkan                                       |
| duration  | number                                          | 5000      | Durasi tampil pesan (dalam milisecond)                            |
| type      | string ('success', 'info', 'warning', 'danger') | 'success' | Tipe tampilan pesan. Biasanya merujuk ke warna pesan              |
| autoClose | bool                                            | true      | Indikator untuk tampilan pesan, apakah hilang otomatis atau tidak |

## Modal
Digunakan untuk membuat tampilan popup contain. Contohnya seperti tampilan informasi detail dalam satu page yang sama.
Code untuk menambahkan **Modal**:
```javascript
import {Modal} from 'component-ui-web-teravin';
```
Contoh penggunaan **Modal** untuk menampilkan info detail:
```javascript
...
this.state = {
  activeModal: true
}
...
<Modal active={activeModal} onClose={() => this.setState({activeModal: false})} >
  <Modal.Header title="Info Detail" />
  <Modal.Body>
    <p>
      Ini detail info
    </p>
  </Modal.Body>
  <Modal.Footer>
    <p>
      <Button
        onClick={() => this.setState({activeModal: false})}
      >Close</Button>
    </p>
  </Modal.Footer>
</Modal>
```
Dari contoh di atas, terlihat bahwa modal terdiri dari bagian `header`, `body`, dan `footer` yang dibungkus oleh `modal` itu sendiri. Berikut custom props dari **Modal**:

| Name        | Type     | Default     | Description                        |
| ----------- | -------- | ----------- | ---------------------------------- |
| active      | bool     | false       | Indikator untuk menampilkan modal  |
| onClose     | function | onClose(){} | Fungsi untuk mengatur active modal |
| customStyle | object   | {}          | Menambah custom style pada modal   |

**Header** untuk menampilkan header atau title, berikut custom props modal header:

| Name            | Type              | Default   | Description                           |
| --------------- | ----------------- | --------- | ------------------------------------- |
| title           | string (required) | -         | Teks judul yang akan tampil di header |
| backgroundColor | string            | '#F5F5F5' | Warna background header               |
| customStyle     | object            | {}        | Menambah custom style pada header     |

**Body** untuk menampilkan contain detail. Di dalam modal body, kita dapat langsung mengatur tampilan contain yang diinginkan sebagai `children`

**Footer** untuk menampilkan contain baik button atau apapun di bagian footer. Sama seperti di bagian `body` tampilan contain footer juga langsung dideklarasikan sebagai `children`.

## Confirmation Modal
Digunakan untuk menampilkan alert konfirmasi berupa modal. Contoh penggunannya seperti pada saat kita akan menghapus sebuah data, kemudian menampilkan konfirmasi terlebih dahulu sebelum melanjutkan action delete data.
Code untuk menambahkan **Confirmation Modal**:
```javascript
import {ModalConfirmation} from 'component-ui-web-teravin';
```
Contoh penggunaan **Confirmation Modal**:
```javascript
...
this.state = {
  activeConfirmation: true
};
...
<ModalConfirmation
  message="Are you sure you want to delete this data?"
  active={activeConfirmation}
  onConfirm={this.onDeleteData.bind(this)}
  onClose={() => this.setState({activeConfirmation: false})}
/>
...
```
Custom props untuk **Confirmation Modal**:

| Name        | Type                                     | Default  | Description                                                                     |
| ----------- | ---------------------------------------- | -------- | ------------------------------------------------------------------------------- |
| active      | bool                                     | -        | Indikator tampil konfirmasi                                                     |
| message     | string                                   | -        | Pesan teks yang akan ditampilkan                                                |
| onConfirm   | function                                 | -        | Fungsi yang dilakukan ketika tombol 'yes' dipilih                               |
| onClose     | function                                 | -        | Fungsi yang dilakukan ketika tombol 'no' atau area diluar modal diklik          |
| colorBtnYes | string ('primary','warning','danger','') | 'danger' | Custom warna untuk tombol 'yes'                                                 |
| loading     | bool                                     | false    | Indikator loading untuk tombol 'yes'                                            |
| headerColor | string                                   | -        | Custom warna untuk background header, default menyesuaikan custom style project |

## Navigation Bar
Digunakan untuk menampilkan navigation bar yang berada di atas page.
Code untuk menambahkan **Navigation Bar**:
```javascript
import {Navbar} from 'component-ui-web-teravin';
```
Contoh penggunaan **Navigation Bar**:
```javascript
<Navbar logo={https://cdn4.iconfinder.com/data/icons/logos-3/600/React.js_logo-512.png}>
  <Navbar.Start>
    <Navbar.Item href="/about">About</Navbar.Item>
    <Navbar.Dropdown title="Media">
      <Navbar.Item href="/photo">Photo</Navbar.Item>
      <Navbar.Item href="/document">Document</Navbar.Item>
    </Navbar.Dropdown>
    <Navbar.Item href="/contact_us">Contact Us</Navbar.Item>
  </Navbar.Start>
  <Navbar.End
    name="Administrator"
    onLogout={this.logout()}
  />
</Navbar>
```
Custom props **Navigation Bar**:

| Name            | Type                           | Default     | Description                       |
| --------------- | ------------------------------ | ----------- | --------------------------------- |
| logo            | string                         | -           | Url image logo                    |
| title           | string                         | -           | Teks untuk judul                  |
| container       | string ('fluid' / 'fullwidth') | 'fullwidth' | Style ukuran navbar               |
| styleNavbar     | object                         | {}          | Custom style navbar               |
| customStyleLogo | object                         | {}          | Menambah custom style pada navbar |

**Navbar Start** digunakan untuk memulai contain navbar dengan rata kiri. Kita dapat langsung meletakkan `navbar item` sebagai children dari `navbar start`.

**Navbar Item** digunakan untuk mendeklarasikan single menu, yang mana tidak terdapat menu child di dalamnya. Custom props **Navbar Item**:

| Name     | Type   | Default | Description           |
| -------- | ------ | ------- | --------------------- |
| href     | string | '#'     | Alamat path menu      |
| isBorder | bool   | false   | Indikator border menu |

**Navbar Dropdown** digunakan untuk mendeklarasikan menu sebagai parent menu yang terdapat menu child di dalamnya berupa `navbar item`. Custom props **Navbar Dropdown**:

| Name     | Type              | Default | Description           |
| -------- | ----------------- | ------- | --------------------- |
| title    | string (required) | -       | Teks menu             |
| isBorder | bool              | false   | Indikator border menu |

**Navbar End** digunakan untuk mendeklarasikan containe dengan rata kanan, biasanya digunakan untuk deklarasi akun, dan logout button. Custom props **Navbar End**:

| Name             | Type     | Default         | Description                                     |
| ---------------- | -------- | --------------- | ----------------------------------------------- |
| date             | string   | -               | Untuk menampilkan tanggal dan waktu secara real |
| showUsername     | bool     | true            | Indikator untuk menampilkan username akun       |
| showLogoutButton | bool     | true            | Indikator untuk menampilkan button logout       |
| onLogout         | function | onLogout(){}    | Fungsi untuk aksi logout                        |
| name             | string   | 'Administrator' | Nama akun yang akan ditampilkan                 |

## Pagination

## Select

## SelectBox

## SelectSearch

## Switch
Digunakan untuk membuat tampilan pilihan berupa *toggle*.
Code untuk menambahkan **Switch**:
```javascript
import {Switch} from 'component-ui-web-teravin';
```
Contoh penggunaan **Switch** pada indikator status *active* dan *inactive*:
```javascript
...
this.state={status: true};
...
onChangeSwitch(data){
  const {checked} = data;
  this.setState({status: checked});
}
...
<Switch
  checked={status}
  name="status"
  onChange={this.onChangeSwitch}
  text={status ? 'Active' : 'Inactive'}
/>
...
```
Custom props **Switch**:

| Name     | Type                | Default      | Description                             |
| -------- | ------------------- | ------------ | --------------------------------------- |
| onChange | function (required) | onChange(){} | Fungsi untuk mengolah data value switch |
| id       | string              | ''           | Id switch                               |
| name     | string              | ''           | Nama dari switch                        |
| value    | string              | ''           | Value switch                            |
| checked  | bool                | false        | Indikator switch                        |
| disabled | bool                | -            | Indikator disable switch                |
| text     | string              | ''           | Label keterangan switch                 |

## Table
Digunakan untuk membuat table.
Code untuk menambahkan **Table**:
```javascript
import {Table} from 'component-ui-web-teravin';
```
Contoh penggunaan **Table**:
```javascript
<Table fullwidth>
  <Table.Head>
    <Table.Label width={52} label="ID"/>
    <Table.Label width={333} label="Name"/>
    <Table.Label width={333} label="Gender"/>
    <Table.Label width={333} label="Status"/>
  </Table.Head>
  <Table.Body>
    <Table.Values>
      <Table.Value width={52}>1</Table.Value>
      <Table.Value width={333}>Raisa Andriana</Table.Value>
      <Table.Value width={333}>Perempuan</Table.Value>
      <Table.Value  width={333}>Active</Table.Value>
    </Table.Values>
    <Table.Values>
      <Table.Value width={52}>2</Table.Value>
      <Table.Value width={333}>Hamish Daud Wyllie</Table.Value>
      <Table.Value width={333}>Laki-laki</Table.Value>
      <Table.Value  width={333}>Inactive</Table.Value>
    </Table.Values>
  </Table.Body>
</Table>
```
Custom props **Table**:

| Name      | Type | Default | Description                                                                                                                   |
| --------- | ---- | ------- | ----------------------------------------------------------------------------------------------------------------------------- |
| fullwidth | bool | false   | Jika true, panjang table full sesuai body, dan sebaliknya                                                                     |
| border    | bool | false   | Jika true, table diberi border. untuk documentasi table bisa dilihat [disini](https://bulma.io/documentation/elements/table/) |
| striped   | bool | false   | Jika true, row pada table berupa striped selang-seling                                                                        |

Custom props **Table Head**:

| Name    | Type   | Default | Description                      |
| ------- | ------ | ------- | -------------------------------- |
| style   | object | {}      | Untuk custom style tag `<thead>` |
| styleTr | object | {}      | Untuk custom style tag `<tr>`    |

Custom props **Table Label**:

| Name          | Type            | Default  | Description                             |
| ------------- | --------------- | -------- | --------------------------------------- |
| label         | string          | -        | Teks label untuk title                  |
| width         | number / string | -        | Panjang kolom                           |
| fontSize      | number / string | 13       | Ukuran font pada teks label             |
| align         | string          | 'center' | Rata teks pada kolom                    |
| fontColor     | string          | '#fff'   | Untuk custom warna pada font            |
| verticalAlign | string          | 'middle' | Untuk custom align text secara vertical |

Custom props **Table Body**:

| Name  | Type   | Default | Description                      |
| ----- | ------ | ------- | -------------------------------- |
| style | object | {}      | Custom style untuk tag `<tbody>` |

Custom props **Table Values**:

| Name       | Type     | Default     | Description                                    |
| ---------- | -------- | ----------- | ---------------------------------------------- |
| style      | object   | {}          | Custom style untuk tag `<tr>`                  |
| onClick    | function | onClick(){} | Fungsi untuk mengatur aksi ketika baris diklik |
| isSelected | bool     | false       | Indikasi row dapat diselect atau tidak         |

Custom props **Table Value**:

| Name        | Type            | Default     | Description                           |
| ----------- | --------------- | ----------- | ------------------------------------- |
| value       | any             | -           | Teks yang tampil pada kolom           |
| colspan     | string          | -           | Untuk merge kolom per baris           |
| bold        | bool            | -           | Untuk tampilan teks bold atau tidak   |
| onClick     | function        | onClick(){} | Fungsi untuk aksi ketika kolom diklik |
| fontSize    | number / string | 13          | Ukuran font pada teks                 |
| align       | string          | 'center'    | Rata pada teks                        |
| color       | string          | -           | Warna pada teks                       |
| height      | number          | -           | Mengatur tinggi kolom                 |
| width       | number / string | -           | Mengatur panjang kolom                |
| paddingLeft | number / string | 14          | Mengatur jarak kiri teks              |

## Table with Select

## Menu Tabs
Digunakan untuk membuat tabs. Untuk jenis-jenis tampilan tabs bisa lihat dokumentasinya [di sini](https://bulma.io/documentation/components/tabs/).
Code untuk menambahkan **Menu Tabs**:
```javascript
import {Tabs} from 'component-ui-web-teravin';
```
Contoh penggunaan **Menu Tabs**:
```javascript
...
this.state={activeMenu: 'music'};
...
onChangeMenu(menu){
  this.setState({activeMenu: menu});
}
...
<Tabs styles="toggle" size="medium" onChangeMenu={this.onChangeMenu} >
  <Tabs.Item name="music" active={activeMenu} title="Music"/>
  <Tabs.Item name="video" active={activeMenu} title="Video"/>
  <Tabs.Item name="document" active={activeMenu} title="Document"/>
</Tabs>
...
```
Custom props **Tabs**:

| Name         | Type                                          | Default          | Description                                                                                                     |
| ------------ | --------------------------------------------- | ---------------- | --------------------------------------------------------------------------------------------------------------- |
| alignment    | string ('center' / 'right')                   | -                | Alignment pada tabs                                                                                             |
| size         | string ('small' / 'medium' / 'large')         | 'small'          | Ukuran tabs                                                                                                     |
| styles       | string ('border' / 'toggle' / 'round-toggle') | -                | Style tampilan tabs. dokumentasi bisa dilihat [di sini](https://bulma.io/documentation/components/tabs/#styles) |
| onChangeMenu | function                                      | onChangeMenu(){} | Fungsi untuk mengatur perubahan menu tabs                                                                       |
| fullwidth    | bool                                          | false            | Ukuran panjang tabs, full atau tidak                                                                            |

## Text

## Title

## Tree
